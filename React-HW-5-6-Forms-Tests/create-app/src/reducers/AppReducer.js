import {
    GET_ITEM,
    ITEM_ADD_TO_FAVORITE,
    ITEM_REMOVE_FROM_FAVORITE,
    GET_FROM_LOCAL_STORAGE_CART,
    GET_FROM_LOCAL_STORAGE_FAVORITE,
    CHECKOUT_FORM_DATA,
} from '../constants/types';
const initialState= {
    itemState:{},
    favoriteStorage:[],
    cartStorage:[],
    checkoutData:[]
};


export default function AppReducer (state=initialState, action){
    switch (action.type){
        case GET_ITEM:
            return {
                ...state,
                ...{itemState: action.payload,}
            };

        case ITEM_ADD_TO_FAVORITE:
            return {
                ...state,
                favoriteStorage: [...state.favoriteStorage, action.payload]

            };
        case ITEM_REMOVE_FROM_FAVORITE:
            return {
                ...state,
                ...{favoriteStorage: action.payload}
            };
        case GET_FROM_LOCAL_STORAGE_CART:
            return {
                ...state,
                ...{cartStorage:  action.payload}
            };

        case GET_FROM_LOCAL_STORAGE_FAVORITE:
            return {
           ...state,
           ...{favoriteStorage:  action.payload}

            };

        case CHECKOUT_FORM_DATA:
            return {
                ...state,
                ...{checkoutData:action.payload}
            };

        default:
            return state
    }

}
