import {ITEM_LIST} from '../constants/types';
const initialState= {
    itemsList:[],
};

export default function CardListReducer (state=initialState, action){
    switch (action.type){

        case ITEM_LIST:
            return {
                ...state,
                ...{itemsList: action.payload,}
            };
        default:
            return state
    }

}
