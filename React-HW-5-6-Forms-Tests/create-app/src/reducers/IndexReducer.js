import {combineReducers} from "redux";
import CardListReducer from "./CardListReducer";
import ModalReducer from "./ModalReducer";
import AppReducer from "./AppReducer";


const rootReducer = combineReducers({
   CardListReducer,
   ModalReducer,
   AppReducer
}
);

export default rootReducer;