import LocalStorageManager from "../services/LocalStorage/Local-storage-manager";
import {
    MODAL_SWITCH_TRUE,
    MODAL_SWITCH_FALSE,
    GET_ITEM,
    ITEM_LIST,
    GET_FROM_LOCAL_STORAGE_CART,
    GET_FROM_LOCAL_STORAGE_FAVORITE,
    CHECKOUT_FORM_DATA,
    CLEAN_CART_STORAGE
} from '../constants/types';
import Services from "../services/Services";

export function modalSwitchTrue(){
    return {
        type:MODAL_SWITCH_TRUE
    }
}
export function modalSwitchFalse(){
    return {
        type:MODAL_SWITCH_FALSE
    }
}


export function getItem(item){
    return {
        type:GET_ITEM,
        payload:item
    }
}

export function addFromLocalStorageCart(){
    const cartListArray = LocalStorageManager.findLocalItems ('CartItem');
    const cartListUpdate=cartListArray.map(x=>x.val);
    if (cartListArray){
        return {
            type:GET_FROM_LOCAL_STORAGE_CART,
            payload:cartListUpdate
        }
    }

}

export function addFromLocalStorageFavorite(){
    const cartListArray = LocalStorageManager.findLocalItems ('Favorite');
    const cartListUpdate=cartListArray.map(x=>x.val);
    if (cartListArray){
        return {
            type:GET_FROM_LOCAL_STORAGE_FAVORITE,
            payload:cartListUpdate
        }
    }

}

export function addItemList(itemList){
    return {
        type:ITEM_LIST,
        payload:itemList
    }
}

export function storeCheckoutInfo(data){
    return {
        type:CHECKOUT_FORM_DATA,
        payload:data
    }
}

export function cleanCardStorage(){
    const cartListArray = LocalStorageManager.findLocalItems ('CartItem');
    cartListArray.forEach(x=>{
        LocalStorageManager.removeFromLocalStorage(x.key)
    });

    return {
        type:CLEAN_CART_STORAGE
    }
}


// HELPER: redux-hunk-usage
export function asyncAddItems(){
    return dispatch =>{
        Services.getList().then(itemList=>{dispatch(addItemList(itemList.ItemList))});
    }
}