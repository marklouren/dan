import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Navigation from "./components/Navigation/Navigation";
import App from './modules/App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux'; // possibility to request info from storage
import {createStore, applyMiddleware, compose} from "redux"
import rootReducer from "./reducers/IndexReducer"
import reduxThunk from 'redux-thunk';

// Redux Debugger
const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            trace: true, traceLimit: 25
        }) : compose;

// LOGGER MIDDLEWARE
// function loggerMiddleware(store){
//     return function(next) {
//         return function (action) {
//             const result = next(action)
//             console.log('Middleware, store', store.getState());
//             return result
//         }
//     }
// }
// THE SAME AS (ES6):
const loggerMiddleware = store => next => action => {
            const result = next(action);
            console.log('Middleware, store', store.getState());
            return result
};

// CREATE STORE:

const store = createStore(rootReducer,
    composeEnhancers(applyMiddleware(reduxThunk, loggerMiddleware)),
    );
/* eslint-enable */

const app = (
    <Provider store={store}>
        <App/>
    </Provider>
);

ReactDOM.render(app,
  document.getElementById('root'));
serviceWorker.unregister();
