import React, {useEffect} from "react";
import CardItem from "../CardItem/CardItem";
import LocalStorageManager from "../../services/LocalStorage/Local-storage-manager";
import Modal from "../ModalPopup";
import './Cart.scss'
import {useSelector, useDispatch} from 'react-redux'
import {getItem, modalSwitchFalse, modalSwitchTrue, addFromLocalStorageCart} from "../../actions/actions";
import FormCheckout from "../Form/Form";

export default function Cart() {

    //states
    const itemState = useSelector((state) => state.AppReducer.itemState);
    const showModal = useSelector((state) => state.ModalReducer.showModal);
    const cartStorage = useSelector((state) => state.AppReducer.cartStorage);
    const dispatch = useDispatch();

    useEffect( ()=> {
     dispatch(addFromLocalStorageCart())
    }, []);


    const modalToggle = () => {
        showModal ? dispatch(modalSwitchFalse()) : dispatch(modalSwitchTrue())};
        const handleModalItem = (item) => {
            dispatch(getItem(item))
        };

        const removeFromLocalStorage=()=>{
            const localStorageKey = 'CartItem'+'-'+itemState.id;
            LocalStorageManager.removeFromLocalStorage(localStorageKey);
        };

        const removeItemFromCart = () => {
            removeFromLocalStorage();
            dispatch(modalSwitchFalse());
            dispatch(addFromLocalStorageCart());
        };

        return (
            <React.Fragment>
                {showModal ? <Modal modalState={modalToggle}
                                    modalItem={itemState}
                                    addToCardBtn={false}
                                    deleteFromCardBtn={true}
                                    removeItemFromCart={removeItemFromCart}
                                    title={"Do you really want to remove this item?"}/> : null}
                <h2 className="listItem-title">Currently in your Cart</h2>
                <div className="Cart-Section-Wrapper">
                <div className="cardListWrapper">
                    {cartStorage.map((x) => {
                        return (
                            <div key={x.id} className="listItem-item">
                                <CardItem cardData={x}
                                          modalState={modalToggle}
                                          closeBtn={true}
                                          itemDisplay={handleModalItem}
                                />
                            </div>
                        )
                    })}
                </div>

                <div className="formWrapper">
                   <FormCheckout/>
                </div>
                </div>

            </React.Fragment>
        )
}


