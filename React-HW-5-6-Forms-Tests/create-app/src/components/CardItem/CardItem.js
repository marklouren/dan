import React, { useState, useEffect} from 'react';
import "./CardItemStyle.scss"
import Button from "../Button/Button";
import {useSelector, useDispatch} from 'react-redux'
import {addFromLocalStorageFavorite} from  "../../actions/actions";
import LocalStorageManager from '../../services/LocalStorage/Local-storage-manager'

 export default function CardItem(props) {
    const {cardData,
        modalState,
        itemDisplay,
        addToCardBtn,
        closeBtn,
        favoriteBtn} = props;

   const [favorite, setFavorite] = useState(false);  //for Star css
   const [btnAdded, setBtnAdded]  = useState(false); //for btn
   const favoriteList =useSelector( (state)=>state.AppReducer.favoriteStorage) ; // getStatusFrom ReduxState
   const dispatch =useDispatch();


    const addToCardHandler=()=>{
        itemDisplay(cardData);
        modalState();
    };

     useEffect(()=>{
         const localStorageKey = 'Favorite'+'-'+cardData.id;
         const localStorageInfo=LocalStorageManager.getFromLocalStorage(localStorageKey);
         localStorageInfo?setFavorite(true):setFavorite(false);

     },[]);

     useEffect( ()=>{
         if (favoriteList.some(x=>x.id===cardData.id)){    //check if item in favorite in order to apply css
             setFavorite(true);}
     },[]);

     const addToFavorite =()=>{
         const localStorageKey = 'Favorite'+'-'+cardData.id;
         LocalStorageManager.pushToLocalStorage( localStorageKey, cardData)
     };
     const removeFromFavorite=()=>{
         const localStorageKey = 'Favorite'+'-'+cardData.id;
         LocalStorageManager.removeFromLocalStorage(localStorageKey)
     };


     const isFavorite =()=>{
         if (!favoriteList.some(x=>x.id===cardData.id)){    //validation
             addToFavorite();
             dispatch(addFromLocalStorageFavorite());
             setFavorite(true)
         } else {
             removeFromFavorite();
             dispatch(addFromLocalStorageFavorite());
             setFavorite(false)
         }
     };






    return(
        <React.Fragment>
            <div className="cardItem">
                <div className="cardItem-row cardItem-star" >
                    <div>{favoriteBtn?<div onClick={isFavorite} className= {(favorite)?"cardItem-addToFavorite added":"cardItem-addToFavorite"}></div>:null}</div>
                    <div> {closeBtn?<div className="cardItem-close-btn" onClick={addToCardHandler}>x</div>:null}</div>
                </div>
                <div className="cardItem-row cardItem-image-wrapper"><img className="cardItem-image" src={cardData.thumbnailUrl} alt="item image"/></div>
                <div className="cardItem-row"><span className="cardItem-description">Title:</span><span className="cardItem-title">{cardData.title}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">Price:</span><span className="cardItem-price">{cardData.price}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">ISBN:</span><span className="cardItem-isbn">{cardData.id}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">Color:</span><span className="cardItem-color">{cardData.color}</span></div>
                <div className="cardItem-row">
                    {addToCardBtn?<Button onClickHandler={addToCardHandler} changeBtnState={setBtnAdded} btnAdded={btnAdded} InnerText="Add to Card"/>:null}</div>
           </div>
        </React.Fragment>

    )
}

