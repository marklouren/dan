import React, {useEffect} from 'react';
import CardItem from "../CardItem/CardItem";
import "./CardListStyle.scss";
import Modal from "../ModalPopup";
import LocalStorageManager from "../../services/LocalStorage/Local-storage-manager";
import {useSelector, useDispatch} from 'react-redux'
import {
    modalSwitchTrue,
    modalSwitchFalse,
    getItem,
    asyncAddItems,
} from "../../actions/actions";

export default  function CardList() {

    // States
    const itemsList = useSelector((state) => state.CardListReducer.itemsList);
    const itemState = useSelector((state) => state.AppReducer.itemState);
    const showModal = useSelector((state) => state.ModalReducer.showModal);
    // Actions
    const dispatch = useDispatch();


        // Get List of Data Redux-hunk
        useEffect(() => {dispatch(asyncAddItems());}, []);
        //handleModal Redux
        const modalToggle = () => {showModal ? dispatch(modalSwitchFalse()):dispatch(modalSwitchTrue());};
        //passItemIntoModal Redux
        const handleModalItem = (item) => {
            dispatch(getItem(item));
        };

    //add Item to localStorageCard
    const addItemToLocalStorage =(item)=>{
        const localStorageKey = 'CartItem'+'-'+item.id;
        LocalStorageManager.pushToLocalStorage( localStorageKey, item)
    };

// check if an item is a favorite
        if (itemsList !== undefined) {
            return (
                <React.Fragment>
                    {showModal ? <Modal modalState={modalToggle}
                                        modalItem={itemState}
                                        addToLocalStorage={addItemToLocalStorage}
                                        addToCardBtn={true}
                                        deleteFromCardBtn={false}
                                        title={"Do you really want to add this Book to Card?"}/> : null}
                    <h2 className="listItem-title">Currently in our store</h2>
                    <div className="cardListWrapper">
                        {itemsList.map((x) => {
                            return (<div key={x.id} className="listItem-item">
                                <CardItem cardData={x}
                                          modalState={modalToggle}
                                          itemDisplay={handleModalItem}
                                          closeBtn={false}
                                          addToCardBtn={true}
                                          favoriteBtn={true}
                                />
                            </div>)
                        })
                        }
                    </div>
                </React.Fragment>
            )
        }
        return (
            <React.Fragment>
                <div>No Valid Data: Items are Undefined</div>
            </React.Fragment>
        )

    }




