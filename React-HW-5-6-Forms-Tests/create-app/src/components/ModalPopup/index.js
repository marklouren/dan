import React, {useEffect, useRef, useState} from 'react';
import isNil from 'lodash/fp/isNil';
import "./ModalPopupStyle.scss"
import CardItem from "../CardItem/CardItem";
import Button from "../Button/Button";

export default function Modal(props){
    const  {title='Default',
     modalItem,
     modalState,
      addToLocalStorage,
     addToCardBtn,
     deleteFromCardBtn,
     removeItemFromCart} = props;

    const modal = useRef(null);



    const addToCart=()=>{
        addToLocalStorage(modalItem);
        modalState();
    };

    useEffect( () => {
        document.addEventListener("click", handlerCloseModal, false);

        return () => {
            document.addEventListener("click", handlerCloseModal, false);
        }
    });

    const handlerCloseModal = (e) => {
        if (!isNil(modal.current)) {
            if (!modal.current.contains(e.target)) {
                modalState()
            }
        }
    };


    return (
        <div  className="modal-window-wrapper">
            <div className="modal-window" ref={modal}>
                <div className="modal-window-header">
                    <div className="modal-window-close" onClick={modalState}>x</div>
                </div>
                <div className="modal-window-body">
                    <div className="modal-window-title">{title}</div>
                    <div className="modal-window-content">
                         {modalItem?<CardItem cardData={modalItem} closeBtn={false} addToCardBtn={false} favoriteBtn={false}/>:null}
                    </div>

                    <div className="modal-window-btn">
                        {addToCardBtn?<Button onClickHandler={addToCart} isModalAddBtn={true} InnerText="Add to Card"/>:null}
                        {deleteFromCardBtn?<Button onClickHandler={removeItemFromCart} InnerText="Remove Item"/>:null }
                        <button className="modal-btn" onClick={modalState}>close</button>
                    </div>
                </div>
            </div>
        </div>

    )
}
