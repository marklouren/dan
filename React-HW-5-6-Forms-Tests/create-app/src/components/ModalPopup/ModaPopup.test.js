import Modal from './index'
import React from "react";
import {configure, shallow, render} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure ({
    adapter:new Adapter()
});


describe( '<Modal/>', ()=> {
    let wrapper;
    beforeEach( ()=>{
        wrapper = shallow(<Modal/>);
    });

    it ('renders without crashing', ()=>{
        shallow(<Modal/>);
    });

    it ('Modal Title should be Default', ()=>{
        const props ={
            title:'Default'
        };
        const modalComponent = render( <Modal {...props}/>);
        expect (modalComponent.find('.modal-window-title').text()).toEqual('Default')
    });

    it ('Modal  State True, should render with Close element', ()=>{
        const props ={
            modalState:true
        };
        const modalComponent = render( <Modal {...props}/>);
        expect (modalComponent.find('.modal-window-close').text()).toEqual('x')
    });

});