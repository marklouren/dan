import './ButtonStyle.scss'
import React from "react";


export default function Button(props){
    const {onClickHandler, InnerText, btnAdded, changeBtnState, isModalAddBtn, disabled, rest} = props;

  const buttonOnclickAction=()=>{
      if (onClickHandler) {
          onClickHandler();
      }
      if (changeBtnState){
          if (isModalAddBtn===true) {
              changeBtnState(false);
          }

      }

  };



    return(
        <React.Fragment>
            {btnAdded?<button className="main-btn main-btn-grey"  disabled={true}>Done</button>:
            <button className="main-btn main-btn-text" disabled={disabled} onClick={buttonOnclickAction}>{InnerText}</button>}
    </React.Fragment>
    )
}