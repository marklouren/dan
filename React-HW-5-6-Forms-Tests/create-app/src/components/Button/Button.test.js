import React from "react";
import {configure, shallow, render} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Button from "./Button";

configure ({
    adapter:new Adapter()
});


describe( '<Button/>', ()=> {
    let wrapper;
    beforeEach( ()=>{
        wrapper = shallow(<Button/>);
    });

    it ('renders without crashing', ()=>{
        shallow(<Button/>);
    });


    it ('Remove Item Should be rendered in the inner Text', ()=>{

        const nextProps= {
            InnerText:'Remove Item',
        };
        wrapper = render(<Button {...nextProps}/>);
        expect (wrapper.text()).toEqual('Remove Item');
    });

    // Check this for me!
    it ('Btn added must be false', ()=>{
        const nextProps = {
            btnAdded:false
        };
        const wrapper =render(<Button {...nextProps}/>);
      expect (wrapper.find('button').hasClass('main-btn-grey')).toEqual(false)

    });



});