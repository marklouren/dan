import React, {useEffect} from "react";
import CardItem from "../CardItem/CardItem";
import './Favorites.scss'
import {useSelector, useDispatch} from "react-redux";
import {addFromLocalStorageFavorite} from "../../actions/actions";

export default function Favorites() {
    const dispatch =useDispatch();

    useEffect( ()=>{
        dispatch(addFromLocalStorageFavorite())
    },[]);
    const favoriteStorage = useSelector(state=> state.AppReducer.favoriteStorage);


    return (
        <React.Fragment>
            <h2 className="listItem-title">Your favorite Items:</h2>
            <div className="cardListWrapper">
                {favoriteStorage.map( (x)=>{
                    return(
                        <div key={x.id} className="listItem-item">
                            <CardItem cardData={x}
                                      favoriteBtn={true}
                                      favoriteStatus={true}
                            />
                        </div>
                    )
                })}
            </div>

        </React.Fragment>
    )

}
