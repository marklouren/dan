import React, {useState} from "react";
import {useSelector, useDispatch} from 'react-redux'
import './Form.scss'
import Button from "../Button/Button";
import Input from "./Input/Input";
import {storeCheckoutInfo,cleanCardStorage, addFromLocalStorageCart} from "../../actions/actions";


export default function FormCheckout (){
    const cartStorage = useSelector((state) => state.AppReducer.cartStorage);
    const dispatch = useDispatch();

    const [isFormValid, setIsFormValid]=useState(false);

    const [formControls, setFormControls]=useState({
        name:{
                value:'',
                type:'text',
                label:'Name',
                errorMessage: 'Enter a correct Name!',
                valid: false,
                touched:false,
                validation: {
                    required: true,
                    minLength: 1,
                }
            },
        lastName: {
            value: '',
            type: 'text',
            label: 'Last Name',
            errorMessage: 'Enter your Last Name',
            valid: false,
            touched: false,
            validation: {
                required: true,
            }
        },
            age: {
                value: '',
                type: 'number',
                label: 'Age',
                errorMessage: 'Enter a correct Age. From 16',
                valid: false,
                touched: false,
                validation: {
                    required: true,
                    minAge: 16
                }
            },
        delivery: {
            value: '',
            type: 'text',
            label: 'Delivery Address',
            errorMessage: 'Enter your shipment address',
            valid: false,
            touched: false,
            validation: {
                required: true,
                minLength: 5,
            }
        },
        phoneNumber: {
            value: '',
            type: 'number',
            label: 'Your Phone Number',
            errorMessage: 'Enter your phone number, Format: 8095556656',
            valid: false,
            touched: false,
            validation: {
                required: true,
                minLength: 7,
            }
        },
    });


    const submitHandler=(event)=>{
        event.preventDefault();
        console.log(submittedData()); // send info to a Console
        dispatch(storeCheckoutInfo(submittedData()));
        dispatch(cleanCardStorage());
        dispatch(addFromLocalStorageCart());
        clearForm();
    };

    // Create an Object for adding to ReduxStorage and Console
    const submittedData=() =>{
        const preparedData = {
            ...formControls
        };
        let submittedDataObject={};
        Object.keys(preparedData).forEach(name=> {
           submittedDataObject[name]=preparedData[name].value;
        });
        const fullSubmittedData = {'Order':cartStorage, 'Contact Info':submittedDataObject};
        return(fullSubmittedData)
    };
    const clearForm =()=> {
        Object.keys(formControls).forEach(name=> {
            formControls[name].value='';

        });
    };



    const validateControl =(value, validation)=>{
        console.log(typeof value);
        if (!validation) {
            return true
        }
        let isValid =true;
        if (validation.required) {
            isValid =value.trim() !=='' && isValid
        }
        if (validation.minAge) {
            if (parseInt(value) <=validation.minAge){
                isValid = false;
            }
        }
        if (validation.minLength){
            isValid = value.length>=validation.minLength &&isValid
        }
        return  isValid

    };
    const onChangeHandler =(event, controlName)=>{
        const newFormControl = {
           ...formControls
        };
        const control = {...newFormControl[controlName]};
        control.value = event.target.value;
        control.touched=true;
        control.valid = validateControl(control.value, control.validation);
        newFormControl[controlName]=control;
        setFormControls(newFormControl);
        let isFormValid = true;
        Object.keys(newFormControl).forEach(name=> {
          isFormValid=newFormControl[name].valid && isFormValid //check if valid each form field
        });
        setIsFormValid(isFormValid)
    };

    const renderInputs=()=>{
        return (Object.keys(formControls)).map((controlName, index)=>{
            const control = formControls[controlName];
            return (
                <Input
                key = {controlName+index}
                type={control.type}
                value ={control.value}
                valid={control.valid}
                touched={control.touched}
                label ={control.label}
                shouldValidate={!!control.validation}
                errorMessage={control.errorMessage}
                onChange={event=>onChangeHandler(event, controlName)}
                />
            )

        });

    };
    return (
        <React.Fragment>

            <div className="form-wrapper">
                <h2>Checkout Form:</h2>
            <form onSubmit={submitHandler} className="form-checkout">
                {renderInputs()}
                <Button InnerText="Checkout" disabled ={!isFormValid} />
                </form>

            </div>
        </React.Fragment>
    )
}