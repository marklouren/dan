import React from "react";
import './InputStyle.scss'


function isInvalid({valid, touched, shouldValidate}){
return !valid && shouldValidate && touched
}

const Input = props=>{
    const inputType = props.type ||'text';
    const htmlFor=`${inputType}-${(Math.random()*1000).toFixed(0)}`;
    let cls =['InputWrapper'];
 if (isInvalid(props)){
     cls.push('invalid')
 }


    return (
        <div className={cls.join(' ')}>
            <label htmlFor={htmlFor}>{props.label}</label>
            <input
                type={inputType}
                id={htmlFor}
                value={props.value}
                onChange={props.onChange}
            />
            {isInvalid(props)
                ?<span>{props.errorMessage || 'Enter a Valid Value'}</span>:null
            }


        </div>
    )
};
export default Input