import React, {Component, useState} from 'react';
export default class LocalStorageManager extends Component{

    static pushToLocalStorage(key, value) {
        const jsonValue = JSON.stringify(value);
        localStorage.setItem(`${key}`, `${jsonValue}`);
    }

    static AddToLocalArrayByKeyLocalStorage(key, value) {
        const receivedValue =  localStorage.getItem(`${key}`);
        let parsedValue  = JSON.parse(receivedValue);
        if (parsedValue===null){
            parsedValue=[];
            parsedValue.push(value);
        } else {
            parsedValue.push(value)
        }
        localStorage.setItem(key, JSON.stringify(parsedValue));
    }

    static removeElementFromArray(key,value){
        const arrayCards = this.getFromLocalStorage(key);
        let updateCards = arrayCards.filter(element => element.value === value);
        this.pushToLocalStorage(key, updateCards)
    }


    static getFromLocalStorage(key) {
        const receivedValue =  localStorage.getItem(`${key}`);
        return JSON.parse(receivedValue);
    }
    static removeFromLocalStorage(key){
        localStorage.removeItem(key);
    }

    static  findLocalItems (query) {
        let i, results = [];
        for (i in localStorage) {
            if (localStorage.hasOwnProperty(i)) {
                if (i.match(query) || (!query && typeof i === 'string')) {
                   let value = JSON.parse(localStorage.getItem(i));
                    results.push({key:i,val:value});
                }
            }
        }
        return results;
    }
}