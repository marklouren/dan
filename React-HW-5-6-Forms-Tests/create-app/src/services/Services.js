
export default class TaskService {
static async getList() {
    return await fetch(`${process.env.PUBLIC_URL}/itemList.json`).then((response) => response.json());
}
}