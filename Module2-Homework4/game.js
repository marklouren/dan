
/*Class Initialization*/
(function startGame(){
    const startButton =document.getElementsByClassName('button-start')[0];
    const difLevel = document.getElementsByClassName('difficulty-level-options')[0];
    startButton.addEventListener('click', ()=>{
        const level = difLevel.options[difLevel.selectedIndex].value;
        new WhackMole(level);
        startButton.style.opacity='0.5';
    })

})();

/*Class Description*/
class WhackMole{
    scorePlayer = 0;
    scoreComputer = 0;
    winnerScore=50;
    playerScoreDisplay = document.getElementsByClassName('player-score-is')[0];
    computerScoreDisplay = document.getElementsByClassName('computer-score-is')[0];

    constructor(level){
        this.level=level;
        this.table =document.getElementsByClassName('game-table')[0];
        this._setTimer(this.level);
        this._showCell;
    }

   _setTimer(level){
        if (level==='easy'){
            this.speed=1500;}
        if (level==='medium'){
            this.speed=1000;}
        if (level==='hard'){
            this.speed=500;}

   }
    _randomize(max){
        return Math.floor(Math.random() * Math.floor(max));
    }

  _playerClick(cell){
          cell.addEventListener('click', ()=>{
                  cell.classList.remove('blue');
                  cell.classList.add('green');
      });
  }
  _computerCheck(cell){
      if (cell.classList.contains('green')){
          this.scorePlayer+=1;
          this.playerScoreDisplay.innerText=`${this.scorePlayer}`;
          this._layout;
      }
      else {
          cell.classList.remove('blue');
          cell.classList.add('red');
          this.scoreComputer+=1;
          this.computerScoreDisplay.innerText=`${this.scoreComputer}`;
          this._layout;

      }
  }
   get _showCell(){
        const table = document.getElementsByClassName('game-table')[0];
        const cells= table.querySelectorAll('tr>td');
        let timerId=setInterval( ()=>{
            let random=this._randomize(cells.length);
            if (!(cells[random].hasAttribute('data-status'))&&
                (this.scorePlayer<this.winnerScore)&&
                (this.scoreComputer<this.winnerScore)){
                cells[random].setAttribute('data-status','checked');
                cells[random].classList.add('blue');

                //USER INTERACTION
                this._playerClick(cells[random]);

                //Computer Interaction
                let timerCheck=setTimeout( ()=>{
                    this._computerCheck(cells[random])
                },this.speed);

            }

        },this.speed+150);
        }

  get _layout() {
        const modal = document.getElementsByClassName('winner-popup')[0];
        const winner = document.getElementsByClassName('popup-name')[0];

        if (this.scorePlayer>=this.winnerScore){
            modal.style.display ='flex';
            winner.innerText='Player Won';
        }
        if (this.scoreComputer>=this.winnerScore){
            modal.style.display ='flex';
            winner.innerText='Computer Won';
        }
       this._refresh;

   }
  get _refresh(){
       const table = document.getElementsByClassName('game-table')[0];
       const cells= table.querySelectorAll('tr>td');
       const startAgain = document.getElementsByClassName('popup-start')[0];
       const modal = document.getElementsByClassName('winner-popup')[0];

       startAgain.addEventListener('click', ()=>{
           modal.style.display='none';
           this.scorePlayer = 0;
           this.scoreComputer = 0;
           this.playerScoreDisplay.innerText='0';
           this.computerScoreDisplay.innerText='0';
           for( let i=0; i<cells.length; i++){
               cells[i].className='';
               cells[i].removeAttribute('data-status')
           }
       })

   }





}

//new WhackMole('easy');