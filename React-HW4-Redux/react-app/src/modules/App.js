import React, {useState, useEffect} from 'react';
import './App.scss';
import CardList from '../components/CardList/CardList';
import Cart from "../components/Cart/index";
import Modal from "../components/ModalPopup";
import Favorites from "../components/Favorites";
import LocalStorageManager from "../services/LocalStorage/Local-storage-manager";
import Navigation from "../components/Navigation/Navigation";
import {connect} from "react-redux";

function App(){
    return (
        <>
            <Navigation/>
        </>
    )

}
function mapStateToProps(state){
    return {
      // user:state.userInfo.user
    }
}

export default connect(mapStateToProps)(App);

