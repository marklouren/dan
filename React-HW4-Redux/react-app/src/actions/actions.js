import {
    MODAL_SWITCH_TRUE,
    MODAL_SWITCH_FALSE,
    GET_ITEM,
    ITEM_LIST,
    ITEM_ADD_TO_CART,
    ITEM_REMOVE_FROM_CART,
    ITEM_ADD_TO_FAVORITE,
    ITEM_REMOVE_FROM_FAVORITE,
    // FAVORITE_SWITCH_TRUE,
    // FAVORITE_SWITCH_FALSE
} from '../constants/types';
import Services from "../services/Services";

export function modalSwitchTrue(){
    return {
        type:MODAL_SWITCH_TRUE
    }
}
export function modalSwitchFalse(){
    return {
        type:MODAL_SWITCH_FALSE
    }
}

// export function isFavoriteTrue(){
//     return {
//         type:FAVORITE_SWITCH_TRUE
//     }
// }
// export function isFavoriteFalse(){
//     return {
//         type:FAVORITE_SWITCH_FALSE
//     }
// }
export function getItem(item){
    return {
        type:GET_ITEM,
        payload:item
    }
}

export function addItemList(itemList){
    return {
        type:ITEM_LIST,
        payload:itemList
    }
}

export function addItemToCart(item){
    return {
        type:ITEM_ADD_TO_CART,
        payload:item,

    }
}

export function removeItemFromCard (removedItem, CardList){
    const itemId = removedItem.id;
    let removeIndex = CardList.map(function(item) {return item.id; }).indexOf(itemId);
    const cartListArray = CardList.filter ((value,index,arr)=>{
        return index !==removeIndex;
    });
    return {
        type:ITEM_REMOVE_FROM_CART,
        payload:cartListArray
    }
}

export function addItemToFavorite(item){
    return {
        type:ITEM_ADD_TO_FAVORITE,
        payload:item,
    }
}

export function itemRemoveFromFavorite(removedItem, CardList){
    const itemId = removedItem.id;
    const removeIndex = CardList.map(function(item) {return item.id; }).indexOf(itemId);
    const FavoriteArray = CardList.filter ((value,index,arr)=>{
        return index !==removeIndex;
    });
    return {
        type:ITEM_REMOVE_FROM_FAVORITE,
        payload:FavoriteArray
    }
}


// HELPER: redux-hunk-usage
export function asyncAddItems(){
    return dispatch =>{
        Services.getList().then(itemList=>{dispatch(addItemList(itemList.ItemList))});
    }
}