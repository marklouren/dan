import React, {useEffect} from 'react';
import CardItem from "../CardItem/CardItem";
import "./CardListStyle.scss";
import Modal from "../ModalPopup";
import {connect} from "react-redux";
import {
    modalSwitchTrue,
    modalSwitchFalse,
    getItem,
    asyncAddItems,
    addItemToCart,
} from "../../actions/actions";

function CardList(props) {

 // Get List of Data Redux-hunk
    useEffect(()=>{
       props.onAsyncAdd();

    },[]);

   //handleModal Redux
    const modalToggle=()=>{
        props.showModal?props.ShowModalSwitchFalse():props.ShowModalSwitchTrue()};

    //passItemIntoModal Redux
    const handleModalItem =(item)=>{
        props.GetItemState(item);
    };
// Add unique items to ReduxStorage
   const addToReduxStorage =()=>{
        if (!props.cartStorage.some(x=>x.id===props.itemState.id)){    //validation
           props.addItemToCart(props.itemState);
        }
   };

   // check if an item is a favorite


    if (props.itemsList !== undefined) {
        return (
            <React.Fragment>
                {props.showModal?<Modal modalState={modalToggle}
                                  modalItem={props.itemState}
                                  addToReduxStorage={addToReduxStorage}
                                  addToCardBtn={true}
                                  deleteFromCardBtn={false}
                                  title={"Do you really want to add this Book to Card?"}/>:null}
                <h2 className="listItem-title">Currently in our store</h2>
              <div className="cardListWrapper">
                    {props.itemsList.map((x) => {
                    return (<div key={x.id} className="listItem-item">
                        <CardItem cardData={x}
                                  modalState={modalToggle}
                                  itemDisplay={handleModalItem}
                                  closeBtn={false}
                                  addToCardBtn={true}
                                  favoriteBtn={true}
                        />
                    </div>)
                })
                }
              </div>
            </React.Fragment>
        )
    }
    return (
        <React.Fragment>
            <div>No Valid Data: Items are Undefined</div>
        </React.Fragment>
    )

}
//listener of the Redux storage
function mapStateToProps(state){    //state from CardlistReducer?
return {
    itemsList:state.CardListReducer.itemsList,   // state.item равняется item
    itemState:state.AppReducer.itemState,
    showModal:state.ModalReducer.showModal,
    cartStorage:state.AppReducer.cartStorage,

}
}
// Dispatching actions to reducers =>
function mapDispatchToProps(dispatch){
 return {
     ShowModalSwitchTrue: () => dispatch(modalSwitchTrue()),
     ShowModalSwitchFalse: () => dispatch( modalSwitchFalse()),
     GetItemState: item => dispatch( getItem(item)),
     onAsyncAdd: ()=>{dispatch(asyncAddItems())},
     addItemToCart: (item)=>{dispatch(addItemToCart(item))}, //  add ItemToReduxStorage CartItem

 }
}
export default connect(mapStateToProps, mapDispatchToProps)(CardList);   // accept tow functions




