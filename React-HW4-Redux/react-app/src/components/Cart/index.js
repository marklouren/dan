import React from "react";
import CardItem from "../CardItem/CardItem";
import Modal from "../ModalPopup";
import './Cart.scss'
import {getItem, modalSwitchFalse, modalSwitchTrue, removeItemFromCard} from "../../actions/actions";
import {connect} from "react-redux";

function Cart(props) {

    const modalToggle=()=>{
        props.showModal?props.ShowModalSwitchFalse():props.ShowModalSwitchTrue()};

    //passItemIntoModal Redux
    const handleModalItem =(item)=>{
        props.GetItemState(item);
    };

// ? Correct or not?  Remove Item from Redux Cart Storage
    const removeItemFromCart=()=>{
        props.removeItemFromCard(props.itemState, props.cartStorage);
        props.ShowModalSwitchFalse();
    };


    return (
        <React.Fragment>
            {props.showModal?<Modal modalState={modalToggle}
                                    modalItem={props.itemState}
                                    addToCardBtn={false}
                                    deleteFromCardBtn={true}
                                    removeItemFromCart={removeItemFromCart}
                                    title={"Do you really want to remove this item?"}/>:null}
            <h2 className="listItem-title">Currently in your Cart</h2>
            <div className="cardListWrapper">

                {props.cartStorage.map( (x)=>{
                    return(
                        <div key={x.id} className="listItem-item">
                            <CardItem cardData={x}
                                      modalState={modalToggle}
                                      closeBtn={true}
                                    //  updateCartList={updateCartList}
                                      itemDisplay={handleModalItem}
                            />
                        </div>
                    )
                })}
            </div>
        </React.Fragment>
    )

}

function mapStateToProps(state) {
    return {
        itemState: state.AppReducer.itemState,
        showModal: state.ModalReducer.showModal,
        cartStorage: state.AppReducer.cartStorage,
    }
}
function mapDispatchToProps(dispatch){
    return {
        ShowModalSwitchTrue: () => dispatch(modalSwitchTrue()),
        ShowModalSwitchFalse: () => dispatch( modalSwitchFalse()),
        GetItemState: item => dispatch( getItem(item)),
        removeItemFromCard: (removedItem, CardList)=>dispatch(removeItemFromCard(removedItem, CardList)),

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
