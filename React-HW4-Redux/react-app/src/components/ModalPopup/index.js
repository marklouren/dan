import React, {useEffect, useRef} from 'react';
import isNil from 'lodash/fp/isNil';
import "./ModalPopupStyle.scss"
import CardItem from "../CardItem/CardItem";

export default function Modal(props){
    const  {title='Default',
     modalItem,
     modalState,
     addToReduxStorage,
     addToCardBtn,
     deleteFromCardBtn,
     removeItemFromCart} = props;

    const modal = useRef(null);

    const addToCart=()=>{
        addToReduxStorage();
        modalState();
    };

    useEffect( () => {
        document.addEventListener("click", handlerCloseModal, false);

        return () => {
            document.addEventListener("click", handlerCloseModal, false);
        }
    });

    const handlerCloseModal = (e) => {
        if (!isNil(modal.current)) {
            if (!modal.current.contains(e.target)) {
                modalState()
            }
        }
    };


    return (
        <div  className="modal-window-wrapper">
            <div className="modal-window" ref={modal}>
                <div className="modal-window-header">
                    <div className="modal-window-close" onClick={modalState}> x</div>
                </div>
                <div className="modal-window-body">
                    <div className="modal-window-title">{title}</div>
                    <div className="modal-window-content">
                         {modalItem?<CardItem cardData={modalItem} closeBtn={false} addToCardBtn={false} favoriteBtn={false}/>:null}
                    </div>

                    <div className="modal-window-btn">
                        {addToCardBtn?<button className="modal-btn" onClick={addToCart}>Add to Cart</button>:null}
                        {deleteFromCardBtn?<button className="modal-btn" onClick={removeItemFromCart}>Remove Item</button>:null}
                        <button className="modal-btn" onClick={modalState}>close</button>
                    </div>
                </div>
            </div>
        </div>

    )
}
