import React, { useState, useEffect} from 'react';
import {connect} from "react-redux";
import "./CardItemStyle.scss"
import {
    addItemToFavorite,
    itemRemoveFromFavorite,

} from "../../actions/actions";



 function CardItem(props) {
    const {cardData,
        modalState,
        itemDisplay,
        addToCardBtn,
        closeBtn,
        favoriteBtn,favoriteStatus} = props;

   const [favorite, setFavorite] = useState(false);  //for Star css

    const addToCardHandler=()=>{
        itemDisplay(cardData);
        modalState();
    };

     useEffect( ()=> {
         setFavorite(favoriteStatus);

     }, []);

     useEffect( ()=>{
         if (props.favoriteList.some(x=>x.id===cardData.id)){    //check if item in favorite in order to apply css
             setFavorite(true);}
     },[]);

     const isFavorite =()=>{
         if (!props.favoriteList.some(x=>x.id===cardData.id)){    //validation
             addToFavoriteReduxStorage();
             setFavorite(true)
         } else {
             props.itemRemoveFromFavorite(cardData, props.favoriteList);
             setFavorite(false)
         }
     };


    const addToFavoriteReduxStorage =()=>{
        if (!props.favoriteList.some(x=>x.id===cardData.id)){
            props.addItemToFavorite(cardData);
        }
    };



    return(
        <React.Fragment>
            <div className="cardItem">
                <div className="cardItem-row cardItem-star" >
                    <div>{favoriteBtn?<div onClick={isFavorite} className= {(favorite)?"cardItem-addToFavorite added":"cardItem-addToFavorite"}></div>:null}</div>
                    <div> {closeBtn?<div className="cardItem-close-btn" onClick={addToCardHandler}>x</div>:null}</div>
                </div>
                <div className="cardItem-row cardItem-image-wrapper"><img className="cardItem-image" src={cardData.thumbnailUrl} alt="item image"/></div>
                <div className="cardItem-row"><span className="cardItem-description">Title:</span><span className="cardItem-title">{cardData.title}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">Price:</span><span className="cardItem-price">{cardData.price}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">ISBN:</span><span className="cardItem-isbn">{cardData.id}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">Color:</span><span className="cardItem-color">{cardData.color}</span></div>
                {addToCardBtn?
                    <div className="cardItem-row">
                        <button className="cardItem-btn" onClick={addToCardHandler}>Add to Card</button>
                    </div>:null
                }
             </div>
        </React.Fragment>

    )
}

function mapStateToProps(state){
    return {
        favoriteList: state.AppReducer.favoriteStorage,

    }
}

function mapDispatchToProps(dispatch){
    return {
        addItemToFavorite: (item)=>{dispatch(addItemToFavorite(item))},
        itemRemoveFromFavorite: (removedItem, CardList)=>dispatch(itemRemoveFromFavorite(removedItem, CardList)),


    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CardItem);