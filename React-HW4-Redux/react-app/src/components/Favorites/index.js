import React from "react";
import CardItem from "../CardItem/CardItem";
import './Favorites.scss'
import {connect} from "react-redux";

function Favorites(props) {

    return (
        <React.Fragment>

            <h2 className="listItem-title">Your favorite Items:</h2>
            <div className="cardListWrapper">
                {props.favoriteStorage.map( (x)=>{
                    return(
                        <div key={x.id} className="listItem-item">
                            <CardItem cardData={x}
                                      favoriteBtn={true}
                                      favoriteStatus={true}
                            />
                        </div>
                    )
                })}
            </div>

        </React.Fragment>
    )

}
function mapStateToProps(state) {
    return {
        favoriteStorage: state.AppReducer.favoriteStorage,
    }
}
function mapDispatchToProps(dispatch){
    return {


    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Favorites);