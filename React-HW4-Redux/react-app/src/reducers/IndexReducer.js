import {combineReducers} from "redux";
import CardListReducer from "./CardListReducer";
import ModalReducer from "./ModalReducer";
import AppReducer from "./AppReducer";
//import FavoriteReducer from "./FavoriteReducer"

const rootReducer = combineReducers({
   CardListReducer,
   ModalReducer,
  // FavoriteReducer,
   AppReducer
}
);

export default rootReducer;