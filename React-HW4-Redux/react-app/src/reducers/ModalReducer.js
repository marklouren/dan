import {MODAL_SWITCH_FALSE, MODAL_SWITCH_TRUE} from "../constants/types";

const initialState= {
    showModal:false
};

export default function ModalReducer (state=initialState, action) {

    switch (action.type) {
        case MODAL_SWITCH_TRUE:
            return  {
                ...state,
                ...{showModal:true}
            };
        case MODAL_SWITCH_FALSE:
            return  {
                ...state,
                ...{showModal:false}
            };
        default:
            return state
    }
}