import {
    GET_ITEM,
    ITEM_ADD_TO_CART,
    ITEM_REMOVE_FROM_CART,
    ITEM_ADD_TO_FAVORITE, ITEM_REMOVE_FROM_FAVORITE
} from '../constants/types';
const initialState= {
    itemState:{},
    favoriteStorage:[],
    cartStorage:[]
};


export default function AppReducer (state=initialState, action){
    switch (action.type){
        case GET_ITEM:
            return {
                ...state,
                ...{itemState: action.payload,}
            };
        case ITEM_ADD_TO_CART:
            return {
                ...state,
                cartStorage: [...state.cartStorage, action.payload] // add item to array Storage
            };
        case ITEM_REMOVE_FROM_CART:
            return {
                ...state,
                ...{cartStorage: action.payload}
               // cartStorage: [...state.slice(0, action.payload), ...array.slice(action.index + 1)]
            };
        case ITEM_ADD_TO_FAVORITE:
            return {
                ...state,
                favoriteStorage: [...state.favoriteStorage, action.payload]

            };
        case ITEM_REMOVE_FROM_FAVORITE:
            return {
                ...state,
                ...{favoriteStorage: action.payload}
            };

        default:
            return state
    }

}
