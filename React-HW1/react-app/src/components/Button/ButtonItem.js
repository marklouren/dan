import React, {Component} from "react";
import './buttonItem.scss'
export default  class Button extends Component {
    constructor(props){
        super(props);
        this.onClickHandler=this.onClickHandler.bind(this);
    }

    onClickHandler() {
       this.props.clickBtn(this.props.number, this.props.action); //передали функцию с родителя - поменяли ее состояние
    }


    render(){
        const text =this.props.text;
        const btnStyle={
            backgroundColor:this.props.color
        };
        return(
            <button className="main-app-btn" style={btnStyle} type="button" onClick={this.onClickHandler}>{text}</button>

        )
    }
}