import React, {Component} from 'react';
import './Modal-Texts.scss'

export default class ModalText extends Component{
    bodyText;
    constructor(props){
        super(props);
        this.option=this.props.option;
        this.modalBodyText();
    }
    modalBodyText(){
        if (this.option ===1){
            this.bodyText =(
                <>
                <div>Once you delete this file, it won't be possible to undo this action.</div>
                <div>Are you sure you want to delete it?</div>
                </>);

        } else {
            this.bodyText = (
                <>
                <div>Hedgehogs are the most dangerous animals</div>
                <div>They can be near You.</div>
                </>)
        }
    }
    render(){
        return (
            <div className="modal-text">{this.bodyText}</div>
        )
    }
}

