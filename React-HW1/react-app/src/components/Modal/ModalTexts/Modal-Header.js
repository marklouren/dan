import React, {Component} from 'react';
import './Modal-Texts.scss'
export default class ModalHeader extends Component{
    headerText;
    constructor(props){
        super(props);
        this.option=this.props.option;
        this.modalHeaderText();
    }

    modalHeaderText(){
        if (this.option ===1){
            this.headerText ='Do you want to delete this file?'
        } else {
            this.headerText ='Please be aware of hedgehogs!!'
        }
    }
    render(){
        return (
            <div className="modal-header-text">{this.headerText}</div>
        )
    }
}