import React, {Component} from 'react';
import ModalHeader from "../ModalTexts/Modal-Header";
import ModalText from "../ModalTexts/Modal-Text";
import ModalOkBtn from "../ModalBtns/Modal-Ok-Btn";
import ModalCancelBtn from "../ModalBtns/Modal-Cancel-Btn";
import ModalCloseBtn from "../ModalBtns/Modal-Close-Btn";
import isNil from 'lodash/fp/isNil';
import './modal.scss'

export default class Modal extends Component {

    constructor(props) {
        super(props);
        this.modalNumber=this.props.modalNumber;
        this.modalAction=this.props.modalAction;
        this.closeBtn =this.props.closeButton;
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
    }
    componentDidMount() {
        document.addEventListener('click', this.handleOutsideClick, false);
    }

    handleOutsideClick(e) {
        if (!isNil(this.modal)) {
            if (!this.modal.contains(e.target)) {
                this.modalAction();
                document.removeEventListener("click", this.handleOutsideClick, false);
            }
        }
    }


    render() {
        return (
            <div className="" ref={node => (this.modal = node)}>
            <div  className="modal-window">
                <div className="modal-window-header">
                    <ModalHeader option={this.modalNumber}/>
                    {(this.closeBtn)? <ModalCloseBtn modalAction={this.modalAction} option={this.modalNumber}/>:null}
                </div>
                <div className="modal-window-body">
                    <div className="modal-window-text">
                        <ModalText  option={this.modalNumber}/>
                    </div>
                    <div className="modal-window-btn">
                        <ModalOkBtn modalAction={this.modalAction} option={this.modalNumber}/>
                        <ModalCancelBtn modalAction={this.modalAction} option={this.modalNumber}/>
                    </div>
                </div>
             </div>
            </div>


        )

    }
}