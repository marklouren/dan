import React, {Component} from 'react';
import './Modal-Btns.scss'

export default class ModalCloseBtn extends Component{
    constructor(props) {
        super(props);
        this.modalAction=this.props.modalAction;
        this.option=this.props.option;
        this.onClickHandler=this.onClickHandler.bind(this)
    }
    onClickHandler(){
        this.modalAction(this.option, false)
    }
    render(){
        return (
            <div onClick={this.onClickHandler} className="modal-close-btn">x</div>
        )
    }
}