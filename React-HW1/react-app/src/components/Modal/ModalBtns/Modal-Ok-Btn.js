import React, {Component} from 'react';
import './Modal-Btns.scss'
export default class ModalOkBtn extends Component{
    constructor(props) {
        super(props);
        this.modalAction=this.props.modalAction;
        this.option=this.props.option;
        this.onClickHandler=this.onClickHandler.bind(this)
    }
    onClickHandler(){
        this.modalAction(this.option, false)
    }
    render(){
        return (
            <button className={(this.option===1)?"modal-btn red":"modal-btn blue" }  onClick={ this.onClickHandler}>Ok</button>
        )
    }
}