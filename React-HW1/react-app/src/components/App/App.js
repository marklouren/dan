import React, {Component} from 'react';
import './app.scss';
import ButtonItem from "../Button/ButtonItem";
import Modal from "../Modal/ModalMain/Modal";
import PropTypes from 'prop-types'

export default class App extends Component {
 constructor(props){
     super(props);
     this.state={
         showModal:false,
         btnValue:''};
      this.openModal=this.openModal.bind(this);
      this.propValidation()
 }
 openModal(number, boolean){
     this.setState({btnValue:number});
     this.setState({showModal:boolean});
     return(this.state);
 };
 propValidation(){
     Modal.propTypes = {
         modalAction: PropTypes.func,
         closeButton:PropTypes.bool
 };
     ButtonItem.propTypes ={
         clickBtn:PropTypes.func,
         action:PropTypes.bool,
         text:PropTypes.string,
         color:PropTypes.string
     }
 }
    /*option instead of (head,text,etc)*/
    render() {
    return (
        <main className={(this.state.showModal)?"main-app-section dark":"main-app-section"}>
            <div className="main-app-section-modal">
          <h1 className="main-app-section-header">React Modals</h1>
                {(this.state.showModal)?<Modal modalAction={this.openModal} modalNumber={this.state.btnValue} closeButton={true}/>:null}
                <div className="main-app-section-buttons">
                  <ButtonItem  clickBtn={this.openModal} action={true} number={1} text={'Open first Modal'} color={'#0086a6'}/>
                 <ButtonItem   clickBtn={this.openModal} action={true} number={2} text={'Open second Modal'} color={'#1e60a6'}/>
                </div>
            </div>
        </main>

    )
  }

}



