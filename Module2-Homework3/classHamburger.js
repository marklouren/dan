
class Hamburger {

    constructor(size, stuffing) {
        this.size = size;
        this.stuffing = stuffing;
        this.calcSize=[];
        this.structure=[];
        this.hamburgerComposition=[];
        this._validationSize;
        this._validationStuffing;
        this._convertStuffing;
    }
     get _validationSize(){
        if (this.size ==='small'){
            this.SIZE_SMALL = {name: 'small', price: 50, cal: 20};
            this.calcSize.push(this.SIZE_SMALL);
        } else if (this.size ==='large'){
            this.SIZE_LARGE = {name: 'large', price: 100, cal: 40};
            this.calcSize.push(this.SIZE_LARGE);
        } else { throw new HamburgerException('Size should be only a string with values: small or large')}
    }
    get _validationStuffing(){
        for (let i=0; i<this.stuffing.length;i++){
            if (this.stuffing[i]!=='cheese'&&this.stuffing[i]!=='potato'&&this.stuffing[i]!=='salad'){
                throw new HamburgerException ('You must have only: cheese, salad or potato among stuffing');
            }
        }
        if (!Array.isArray(this.stuffing)||this.stuffing.length === 0){
            throw new HamburgerException ('Stuffing should be an array with at least one element')
        }
    }
    get _convertStuffing(){
        if (this.stuffing.includes('cheese')) {
            this.STUFFING_CHEESE = {name: 'cheese', price: 10, cal: 20};
            this.structure.push(this.STUFFING_CHEESE)
        }
        if (this.stuffing.includes('salad')) {
            this.STUFFING_SALD = {name: 'salad', price: 20, cal: 5};
            this.structure.push(this.STUFFING_SALD)
        }
        if (this.stuffing.includes('potato')) {
            this.STUFFING_POTATO = {name: 'potato', price: 15, cal: 10};
            this.structure.push(this.STUFFING_POTATO)
        }
    }
    /* Добавить добавку к гамбургеру*/
     addTopping(topping){
        this.topping=topping;
        this.toppingArray=[];
        if (this.topping.length>2){
            throw new HamburgerException ('You can not have more than 2 elements in toppings')
        }
        if (this.topping.includes('mayo')){
            this.TOPPING_MAYO = {name:'mayo', price: 20, cal:5};
            this.toppingArray.push( this.TOPPING_MAYO);
        }
        if (this.topping.includes('spice')){
            this.TOPPING_SPICE=  {name:'spice', price: 20, cal:5};
            this.toppingArray.push( this.TOPPING_SPICE);
        }
        return this.toppingArray;
    }
    /* Remove Topping */
    removeTopping(topping){
        this.topping=topping;
        if ( this.topping.includes('mayo')){
            delete this.TOPPING_MAYO;
            this.toppingArray=this.toppingArray.filter((item)=>{return item.name !=='mayo';});
        }
        if ( this.topping.includes('spice')){
            delete this.TOPPING_MAYO;
            this.toppingArray=this.toppingArray.filter((item)=>{return item.name !=='spice';});
        }
        return this.toppingArray;
    }
    /*Получить список добавок*/
    get getToppings(){
        return this.toppingArray;
    }
    /* Узнать размер гамбургера*/
    get getSize (){
        return this.size;
    };
    /* Узнать начинку гамбургера*/
    get getStuffing(){
        for (let i = 0; i < (this.structure).length; i++) {
            this.hamburgerComposition.push(this.structure[i].name);
        }
        if (this.topping.length > 0) {
            this.hamburgerComposition = this.hamburgerComposition.concat(this.topping);
        }
        return this.hamburgerComposition;
    }
   /*Узнать цену гамбургера*/
    get calculatePrice(){
        this.price = 0;
        this.price+=this.calcSize[0].price;  //size
        this.structure.forEach((item)=>{this.price+=item.price;}); //stuffing
        this.toppingArray.forEach((item)=>{this.price+=item.price;}); // toppings
        return this.price;
    }
    /* Узнать калорийность*/
    get calculateCalories(){
        this.cal = 0;
        this.cal+=this.calcSize[0].cal;  //size
        this.structure.forEach((item)=>{this.cal+=item.cal;}); //stuffing
        this.toppingArray.forEach((item)=>{this.cal+=item.cal;}); // toppings
        return this.cal;
    }

}

// ERROR Handling:
function HamburgerException (message) {
    this.name= 'Hamburger Exception Function';
    this.message = message;
}
HamburgerException.prototype = Error.prototype;
// test

const burger = new Hamburger('large', ['potato', 'salad', 'cheese']);

console.log('Display Variable CHEESE: ');
console.log(burger.STUFFING_CHEESE);
console.log('ADD TOPPING: MAYO, SPICE: ');
console.log(burger.addTopping(['mayo', 'spice']));
console.log('REMOVE TOPPING: SPICE: ');
console.log(burger.removeTopping(['spice']));
console.log('LIST TOPPINGS: ');
console.log(burger.getToppings);
console.log('Get SIZE: ');
console.log(burger.getSize);
console.log('Get List of Hamburger ingredients');
console.log(burger.getStuffing);
console.log('Get Hamburger Price');
console.log(`${burger.calculatePrice} Tugriks`);
console.log('Get Hamburger Calories');
console.log(`${burger.calculateCalories} Calories`);
