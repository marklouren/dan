/*initial setup */
const addColumnButton= document.querySelector('.add-column-button');
const board =document.getElementsByClassName('board-container')[0];
const addColumnName=document.getElementsByClassName('add-column-name')[0];
const columnNameButton=document.getElementsByClassName('add-column-name-button')[0];
const inputNameColumn =document.getElementsByClassName('add-column-name-input')[0];

/*Class Initialization */
(function createColumn() {
    let dataCounter=0;
    addColumnButton.addEventListener('click', () => {
        addColumnName.classList.add('show');
    });
    columnNameButton.addEventListener('click', () => {
        let columnName = inputNameColumn.value;
        new TrelloColumn(columnName,  dataCounter);
        dataCounter+=1;
        addColumnName.classList.remove('show');
    })
})();

/* Instance of Class*/
class TrelloColumn {

    constructor(columnName,  dataCounter) {
        this.columnName=columnName;
        this.dataCounter=dataCounter;
        this._createColumn;
        this.cardButton = document.getElementsByClassName('card-button')[this.dataCounter];
        this.cardBody = document.getElementsByClassName('card-body')[this.dataCounter];
        this.cardConfirmButton =document.getElementsByClassName('card-confirm-button')[this.dataCounter];
        this.cardInput=document.getElementsByClassName('card-name-input')[this.dataCounter];
        this.cardFieldSection=document.getElementsByClassName('card-name-field')[this.dataCounter];
        this.cardSort=document.getElementsByClassName('card-sort-button')[this.dataCounter];
        this._cardText='';
        this._addNewCard;
        this._sortButtonClick;
    }

    get _createColumn(){
        let column=document.createElement('div');
        column.classList.add('card');
        column.setAttribute('data-column-counter', this.dataCounter);
        column.innerHTML=`<div class="card-title"><span>${this.columnName}</span><button class='card-sort-button'>Sort</button></div>
        <ul class="card-body">
        </ul>
        <div class="card-name-field">
            <textarea class="card-name-input" type="text" name="card-name"></textarea>
            <button class="card-confirm-button">ok</button>
        </div>
        <button class="card-button">Add a Card</button>`;
        board.append(column);
    }
    /* Add New Card Functionality */
   get  _addNewCard(){
        this.cardButton.addEventListener('click',()=>{
            this._showCardInput;
        });
       this._getCardValue;
    }
    get  _showCardInput(){
         this.cardFieldSection.classList.add('show');
    }
   get _removeCardInput(){
       this.cardFieldSection.classList.remove('show')
   }
     _createCard(el){
         let card=document.createElement('li');
         card.classList.add('card-body-item');
        // card.setAttribute('draggable', 'true');
         card.innerHTML=el;
         this.cardBody.append(card);

         // LAUNCH DRAG AND DROP FUNCTIONALITY
         this._sortable(this.cardBody);

   }
  get  _getCardValue(){
        this.cardConfirmButton.addEventListener('click', ()=>{
            this._cardText=this.cardInput.value;
            this._createCard(this._cardText);
            this._removeCardInput
        })
    }

// Finished Add New Card Functionality

// Sort Card Items Functionality
    get _sortButtonClick(){
        this.cardSort.addEventListener('click', ()=>{
                // convert all Card items into Array
                let cardCounter = [].map.call(this.cardBody.querySelectorAll('.card-body-item'), function(el) {
                    return el.innerHTML;
                });
                //remove All items
               const removeElements = (elms) => elms.forEach(el => el.remove());
               removeElements(this.cardBody.querySelectorAll(".card-body-item"));

               //Sort Items Case Sensitive
               const sortedCards=cardCounter.sort(function (a, b) {
                   return a.toLowerCase().localeCompare(b.toLowerCase());
               });
               // Added Sorted Cards into CardBody
              sortedCards.forEach(el=>{
                  this._createCard(el);
            });
        })
    }

    // Drag and Drop Functionality///////////////////////////////////////////////////////////////////////

    _sortable(list1) {
        let dragEl;
        // Делаем всех детей перетаскиваемыми
        [].slice.call(list1.children).forEach(function (itemEl) {
            itemEl.draggable = true;
        });

        // Фнукция отвечающая за сортировку
        function _onDragOver(evt) {
            evt.preventDefault();
            evt.dataTransfer.dropEffect = 'move';

            let target = evt.target;
            if (target && target !== dragEl && target.nodeName === 'LI') {
                // Сортируем
                list1.insertBefore(dragEl, target.nextSibling || target);
            }
        }
        // Окончание сортировки
        function _onDragEnd(evt) {
            evt.preventDefault();
            dragEl.classList.remove('ghost');
            list1.removeEventListener('dragover', _onDragOver, false);
            list1.removeEventListener('dragend', _onDragEnd, false);

        }
        // Начало сортировки
           list1.addEventListener('dragstart', function (evt) {
            dragEl = evt.target; // Запоминаем элемент который будет перемещать

            // Ограничиваем тип перетаскивания
            evt.dataTransfer.effectAllowed = 'move';
            evt.dataTransfer.setData('Text', dragEl.textContent);

            // Пописываемся на события при dnd
            list1.addEventListener('dragover', _onDragOver, false);
            list1.addEventListener('dragend', _onDragEnd, false);

            setTimeout(function () {
                // Если выполнить данное действие без setTimeout, то перетаскиваемый объект, будет иметь этот класс.
                dragEl.classList.add('ghost');
            }, 0)
        }, false);
    };

}

