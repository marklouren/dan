import React from "react";
import {BrowserRouter, Route, Link} from "react-router-dom"
//import createBrowserHistory from 'history/createBrowserHistory'
import Favorites from "../Favorites"
import Cart from "../Cart";
import App from "../../modules/App";
import './Navigation.scss'
import { createBrowserHistory } from "history";
import CardList from "../CardList/CardList";
const history =createBrowserHistory();

export default function Navigation(){

    return(
        <BrowserRouter history={history}>
                <nav className="navigation">
                <ul className="navigation-menu">
                    <li className="navigation-menu-item"><Link to="/">Catalog</Link></li>
                    <li className="navigation-menu-item"><Link to="/cart">Cart</Link></li>
                    <li className="navigation-menu-item"><Link to="/favorites">Favorites</Link></li>
                </ul>
                </nav>
            <Route exact path="/" component={CardList}/>
            <Route exact path="/cart" component={Cart}/>
            <Route exact path="/favorites" component={Favorites}/>
        </BrowserRouter>
    )
}