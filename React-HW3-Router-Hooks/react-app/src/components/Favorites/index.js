import React, {useEffect, useState} from "react";
import LocalStorageManager from "../../services/LocalStorage/Local-storage-manager";
import CardItem from "../CardItem/CardItem";
import './Favorites.scss'

export default function Favorites({ rest}) {
 const [favoriteList, setFavoriteList] = useState([]);

  useEffect( ()=> {
      const favoriteListArray = LocalStorageManager.findLocalItems ('Favorite');
     if (favoriteListArray){
         setFavoriteList(favoriteListArray)
     }
  }, []);

 const updateFavoriteList = ()=>{
     const favoriteListArray = LocalStorageManager.findLocalItems ('Favorite');
     setFavoriteList(favoriteListArray);
  };


    return (
        <React.Fragment>

            <h2 className="listItem-title">Your favorite Items:</h2>
            <div className="cardListWrapper">
                {favoriteList.map( (x)=>{
                    return(
                        <div key={x.key} className="listItem-item">
                            <CardItem cardData={x.val}
                                      favoriteBtn={true}
                                      updateFavoriteList = {updateFavoriteList}
                            />
                        </div>
                    )
                })}
            </div>


        </React.Fragment>
    )

}