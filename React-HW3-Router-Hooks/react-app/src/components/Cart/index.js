import React, {useEffect, useState} from "react";
import LocalStorageManager from "../../services/LocalStorage/Local-storage-manager";
import CardItem from "../CardItem/CardItem";
import Modal from "../ModalPopup";
import './Cart.scss'

export default function Cart() {
    const [cartList, setCartList] = useState([]);

    //handleModal
    const [showModal, setShowModal] = useState(false);
    const modalToggle=()=>setShowModal(!showModal);

    //passItemIntoModal
    const [itemState, itemStateDisplay] = useState({});
    const handleModalItem =(item)=>itemStateDisplay(item);

    useEffect( ()=> {
        const cartListArray = LocalStorageManager.findLocalItems ('CartItem');
        if (cartListArray){
            setCartList(cartListArray)
        }
    }, []);


    const updateCartList= ()=>{
        const cartListArray = LocalStorageManager.findLocalItems ('CartItem');
        if (cartListArray){
            setCartList(cartListArray)
        }
    };

    const removeItemFromCart=()=>{
        const localStorageKey = 'CartItem'+'-'+itemState.id;
        console.log(localStorageKey);
        LocalStorageManager.removeFromLocalStorage(localStorageKey);
        updateCartList();
        modalToggle();
    };


    return (
      <React.Fragment>
          {showModal?<Modal modalState={modalToggle}
                            modalItem={itemState}
                            addToCardBtn={false}
                            deleteFromCardBtn={true}
                            removeItemFromCart={removeItemFromCart}
                            title={"Do you really want to remove this item?"}/>:null}
          <h2 className="listItem-title">Currently in your Cart</h2>
          <div className="cardListWrapper">
              {cartList.map( (x)=>{
                  return(
                      <div key={x.key} className="listItem-item">
                          <CardItem cardData={x.val}
                                    modalState={modalToggle}
                                    closeBtn={true}
                                    updateCartList={updateCartList}
                                    itemDisplay={handleModalItem}
                          />
                      </div>
                  )
              })}
          </div>


    </React.Fragment>
     )

}