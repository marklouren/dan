import React, {useEffect, useState} from 'react';
import CardItem from "../CardItem/CardItem";
import "./CardListStyle.scss";
import Modal from "../ModalPopup";
import LocalStorageManager from "../../services/LocalStorage/Local-storage-manager";
import Services from "../../services/Services";

export default function CardList() {

    const [items, setItems] = useState([]);

    useEffect(()=>{
        Services.getList()
            .then(itemList=>{
                setItems(itemList.ItemList)});
    },[]);


   //handleModal
    const [showModal, setShowModal] = useState(false);
    const modalToggle=()=>setShowModal(!showModal);

    //passItemIntoModal
    const [itemState, itemStateDisplay] = useState({});
    const handleModalItem =(item)=>itemStateDisplay(item);

    //add Item to localStorageCard
    const addItemToLocalStorage =(item)=>{
        const localStorageKey = 'CartItem'+'-'+item.id;
        LocalStorageManager.pushToLocalStorage( localStorageKey, item)
    };

    if (items !== undefined) {
        return (
            <React.Fragment>
                {showModal?<Modal modalState={modalToggle}
                                  modalItem={itemState}
                                  addToLocalStorage={addItemToLocalStorage}
                                  addToCardBtn={true}
                                  deleteFromCardBtn={false}
                                  title={"Do you really want to add this Book to Card?"}/>:null}
                <h2 className="listItem-title">Currently in our store</h2>
              <div className="cardListWrapper">
                {items.map((x) => {
                    return (<div key={x.id} className="listItem-item">
                        <CardItem cardData={x}
                                  modalState={modalToggle}
                                  itemDisplay={handleModalItem}
                                  closeBtn={false}
                                  addToCardBtn={true}
                                  favoriteBtn={true}
                                  updateFavoriteList ={()=>{}} />
                    </div>)
                })
                }
              </div>
            </React.Fragment>
        )
    }
    return (
        <></>
    )
} 





