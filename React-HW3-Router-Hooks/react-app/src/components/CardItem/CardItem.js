import React, {Component, useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import LocalStorageManager from '../../services/LocalStorage/Local-storage-manager'
import "./CardItemStyle.scss"
import Services from "../../services/Services";


export default function CardItem(props) {
    const {cardData,
        modalState,
        itemDisplay,
        addToCardBtn,
        closeBtn,
        favoriteBtn,
        updateFavoriteList} = props;

    const [favorite, setFavorite] = useState(false);
    const addToCardHandler=()=>{
        itemDisplay(cardData);
        modalState();
    };

    useEffect(()=>{
       const localStorageKey = 'Favorite'+'-'+cardData.id;
       const localStorageInfo=LocalStorageManager.getFromLocalStorage(localStorageKey);
       localStorageInfo?setFavorite(true):setFavorite(false);

    },[]);
    const isFavorite = ()=> {
        if (favorite===false){
            setFavorite(true);
            addToFavorite();
            updateFavoriteList();
        } else {
            setFavorite(false);
            removeFromFavorite();
            updateFavoriteList();
        }
    };

    const addToFavorite =()=>{
        const localStorageKey = 'Favorite'+'-'+cardData.id;
        LocalStorageManager.pushToLocalStorage( localStorageKey, cardData)
    };
    const removeFromFavorite=()=>{
        const localStorageKey = 'Favorite'+'-'+cardData.id;
        LocalStorageManager.removeFromLocalStorage(localStorageKey)
    };



    return(
        <React.Fragment>
            <div className="cardItem">
                <div className="cardItem-row cardItem-star" >
                    <div>{favoriteBtn?<div onClick={isFavorite} className= {(favorite)?"cardItem-addToFavorite added":"cardItem-addToFavorite"}></div>:null}</div>
                    <div> {closeBtn?<div className="cardItem-close-btn" onClick={addToCardHandler}>x</div>:null}</div>
                </div>
                <div className="cardItem-row cardItem-image-wrapper"><img className="cardItem-image" src={cardData.thumbnailUrl} alt="item image"/></div>
                <div className="cardItem-row"><span className="cardItem-description">Title:</span><span className="cardItem-title">{cardData.title}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">Price:</span><span className="cardItem-price">{cardData.price}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">ISBN:</span><span className="cardItem-isbn">{cardData.id}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">Color:</span><span className="cardItem-color">{cardData.color}</span></div>
                {addToCardBtn?
                    <div className="cardItem-row">
                        <button className="cardItem-btn" onClick={addToCardHandler}>Add to Card</button>
                    </div>:null
                }
             </div>
        </React.Fragment>

    )
}
