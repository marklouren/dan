// SECTION OUR SERVICES //

/*Initial Setup*/
const serviceDescription =$('.services-menu-description');
serviceDescription.hide();
serviceDescription.eq(0).show();

$('.feature').on('click',function(event){
    /*TAB BAR */
    $('.services-menu div.feature').removeClass('active');
    $(this).addClass('active');
    /* TAB DESCRIPTION */
    $('.services_tabs_content div.services-menu-description').hide();
    const tab_id =$(this).attr('data-tab');
    $("#"+tab_id).show();
});

// SECTION Our Amazing Work //

/* Filer Images with TABS */
function filter(value){
    const list=$('.our-work-gallery-item');
    $(list).fadeOut("fast");
    if (value==='all'){
        $(list).each(function(){
            $(this).delay(100).slideDown('fast');
        });
    } else {
        $(".our-work-gallery-item[data-category="+value+"]").each(function(){
            $(this).delay(100).slideDown('fast');
        });
    }
}
$('.our-work-menu').on('click', function(event){
    $('.our-work-item').removeClass('focus-item');
    const target =$(event.target);
    if (target.is('.our-work-item')) {
        $(target).addClass('focus-item');
        const value = $(target).attr('data-tab');
        filter(value)
    }
});

/* Loader  */

//set up
$('.block2').addClass('hide');  /// REPLACE BY CLASS
$('.block3').addClass('hide');
$('.radial').hide();

let buttonClick=0;

$('.btn_load').on('click', function(){
    buttonClick+=1;
    // button change
    function showLoader(){
        $('.btn_load').hide();
        $('.radial').show();
    }
    if (buttonClick === 2){
        let counter= 0;
        let loader1 = setInterval( function(){
            showLoader();
            counter += 1;
            if(counter === 500) {
                clearInterval(loader1);
                $('.btn_load').hide();
                $('.radial').hide();
                $('.block3').removeClass('hide').addClass('show');
            }
        }, 4);

    } else {
        let timesRun =0;
        let loader = setInterval( function(){
            showLoader();
            timesRun += 1;
            if(timesRun === 500) {
                clearInterval(loader);
                $('.btn_load').show();
                $('.radial').hide();
                $('.block2').removeClass('hide').addClass('show');
            }
        }, 4);
    }
});

// SECTION TESTIMONIALS SLIDER  - SLICK//

$(window).on('load',function(){
    $('.slider_for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        asNavFor: '.slider_nav',
    });
    $('.slider_nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider_for',
        centerMode: true,
        focusOnSelect: true,
        variableWidth: true,
    });
});


//LOAD MORE BEST IMG //

const bestImgBtn =   $('.btn_load_best_img');
const bestImgLoader = $('.radial-best-img');
$(bestImgLoader).hide();

function showLoaderBestImg(){
    $(bestImgBtn).hide();
    $(bestImgLoader).show();
}
$(bestImgBtn).on('click', function(){
    let timesRun = 0;
    let loader = setInterval( function(){
        showLoaderBestImg();
        timesRun += 1;
        if(timesRun === 500) {
            clearInterval(loader);
            $(bestImgLoader).hide();
            $('.best-img-2').removeClass('hide').addClass('show');
        }
    });
});

// Masonry //

$(window).load(function () {
    let $container = $(".masonry-container");
    $container.imagesLoaded(function(){
        $container.masonry( {
            columnWidth: 390,
            itemSelector: ".item",
            gutter: 17,
            /*percentPosition:true,*/
            fitWidth: true,
            isFitWidth: true,
        });
    });
});

