let menuIcon = document.getElementsByClassName('js_nav-bar-button')[0];
let menuBlock = document.getElementsByClassName('js_navigation-menu-items')[0];
let faIcon = document.getElementsByClassName('fa')[0];

function openMenu() {
    if (menuBlock.style.display === "block") {
        menuBlock.style.display = "none";
    } else {
        menuBlock.style.display = "block";
    }
}
function changeIcon(){
    if (faIcon.classList.contains('fa-bars')){
        faIcon.classList.remove('fa-bars');
        faIcon.classList.add('fa-times');

    }else {
        faIcon.classList.add('fa-bars');
    }
}

menuIcon.addEventListener('click',()=>{
    openMenu();
    changeIcon();
});