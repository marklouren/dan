document.getElementById('#listMovies').addEventListener('click', ()=>{
    ajaxGet();
});

let container = document.getElementsByClassName('container')[0];

async function ajaxGet(){
    let fetchRequest = await fetch('https://swapi.co/api/films/');
    if (fetchRequest.ok) {
        let fetchAnswer = await fetchRequest.json();
        let receivedValue = fetchAnswer.results;
        for (let i = 0; i < receivedValue.length; i++) {
            const movieItem = document.createElement('div');
            movieItem.classList.add('movie');
            movieItem.setAttribute('data-name', receivedValue[i].title);
            movieItem.innerHTML = `
         <h3 class="movie-item movie-id">${receivedValue[i].episode_id}</h3>
        <div class="movie-item movie-name">${receivedValue[i].title}</div>
        <p class="movie-item movie-opening-crawl">${receivedValue[i].opening_crawl}</p>
        <div class="movie-item movie-heroes">
            <h3 class="movie-item movie-heroes-title">List of Characters:</h3>
            <ul class="movie-heroes-list">
            </ul>
        </div>`;
            container.appendChild(movieItem);
            /*===========================   ADDED LIST OF MOVIES PROMISE.ALL */
            let movieList=document.getElementsByClassName('movie-heroes-list')[i];
            let characterListUrl = receivedValue[i].characters; // list of urls
            Promise.all(characterListUrl.map(u => fetch(u)))
                .then(responses => Promise.all(responses.map(res => res.json())))
                .then( function(results){
                    let namesArray = [];
                    for (let i = 0; i < results.length; i++){
                        namesArray.push(results[i].name);
                    }
                    for (let i = 0; i < namesArray.length; i++){
                        let person =document.createElement('li');
                        person.innerText=namesArray[i];
                        movieList.appendChild(person)
                    }

                });

        }
    }
    else
        {
            console.log("Ошибка HTTP: " + fetchRequest.status);
        }
}

