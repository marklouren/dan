
 let tab = document.getElementsByClassName('tabs-title');
let tabContent =document.querySelectorAll('ul.tabs-content>li');
 let tabItems =  Array.prototype.slice.call(tab);
 let tabContentArray = Array.prototype.slice.call(tabContent);

 /*  in order to hide all TABS except first one without CSS - optional;
 window.onload=function () {
     tabContentArray.forEach((x)=>x.classList.add('hide'));
     tabContentArray[0].classList.remove('hide');
     tabContentArray[0].classList.add('show');
 };
*/

 for (let i=0; i<tabItems.length; i++) {
     tabItems[i].addEventListener('click',
         function () {
             tabItems.forEach((x) => {
                 if (x.classList.contains('active')) {
                     x.classList.remove('active')
                 }
                 tabItems[i].classList.add('active');
                 tabContentArray.forEach((x) => {
                     if (!x.classList.contains('hide')) {
                         x.classList.add('hide')
                     }
                 });
                 tabContentArray[i].classList.remove('hide');
                 tabContentArray[i].classList.add('show');
             });
         })
 }



