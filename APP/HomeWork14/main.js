
$('ul.tabs li').click(function(){

    $('ul.tabs li').removeClass('active');
    $('ul.tabs-content li').hide();
    let tab_id = $(this).attr('data-tab');
    $(this).addClass('active');
    $("#"+tab_id).show();  //#id=tab_id var
});


/*
// Solution with Classes:
        $('ul.tabs li').click(function(){

            $('ul.tabs li').removeClass('active');
           $('ul.tabs-content li').removeClass('show').addClass('hide');

            let tab_id = $(this).attr('data-tab');
            $(this).addClass('active');
            $("#"+tab_id).addClass('show').removeClass('hide');  //#id=tab_id var
        });
*/