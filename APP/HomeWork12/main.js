// Initialization
let imgList=document.getElementsByClassName('image-to-show');

//buttons
let btnStop=document.getElementsByClassName('stop')[0];
let btnContinue=document.getElementsByClassName('continue')[0];

//timer fields:
let sec=document.getElementById('sec');
let ml_sec=document.getElementById('ml_sec');

// Timer

function timer (count){
    let period=count;
    let counter= setInterval(timer, 4);
    function timer(){
        if (period<=4){
            clearInterval(counter);
        }
        period=period-4;
        sec.innerText=`${ Math.floor(period/1000)}`;
        ml_sec.innerText=`${period}`;

    }
}

/* Slider */
let time =10000;
let currentSlide =0;
let SlideInterval =setInterval(nextSlide, time);

timer(time);
function nextSlide(){
    timer(time);
    imgList[currentSlide].classList.add('hide');
    currentSlide = ( currentSlide + 1) % imgList.length;   // % => if counter === imgList.length
    imgList[ currentSlide].classList.remove('hide');


}
/* STOP -PLAY Functionality */
let playing = true;

function pauseSlider(){
    playing=false;
    clearInterval(SlideInterval);
}
function playSlider(){
    playing=true;
    timer(time);
    SlideInterval = setInterval(nextSlide, time);
}
/* Button Events */
btnStop.addEventListener('click', ()=>{
pauseSlider()});
btnContinue.addEventListener('click', ()=>{
 playSlider()});


