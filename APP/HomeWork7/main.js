/*
Создать функцию, которая будет принимать на вход массив.
Каждый из элементов массива вывести на страницу в виде пункта списка
Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
Примеры массивов, которые можно выводить на экран:
 */

let array= ['1', '2', '3', 'sea', 'user', ['sub1','sub2','sub3'], 45, ['sublist 1', 'sublist 2'], 23];
function displayList(array) {
    let listElement;
    let Sublist, ulElement;
    let myList = document.getElementById('myList');

    let fragment = new DocumentFragment();
    array.map((x) => {
 if(x instanceof Array) {
     listElement = document.createElement('li');
     listElement.style.listStyleType='none'; // if you don't need bullets
     ulElement = document.createElement('ul');
     listElement.appendChild(ulElement);
     for (let n of x) {
         Sublist = document.createElement('li');
         Sublist.innerHTML = `${n}`;
         ulElement.insertAdjacentElement('afterbegin', Sublist)
     }
     fragment.appendChild(listElement);
 }
 else {
     listElement = document.createElement('li');
     listElement.innerHTML = `${x}`;
     fragment.appendChild(listElement);
 }
 });
    myList.prepend(' ', fragment);

    // Set up a Timer

    let seconds = document.getElementById("countdown").textContent;  //Display Timer
    let countdown = setInterval(function() {
        seconds--;
        document.getElementById("countdown").textContent = seconds;
        if (seconds <= 0) clearInterval(countdown);
    }, 1000);
    setTimeout(() => myList.remove(), 10000);  //delete list

}

displayList(array);


