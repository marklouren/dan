let student = {};
let subject;
let grade;

student['name'] = prompt('Enter your name');
student['last name'] =prompt('Enter your last name');
student['table'] = [];
subject = prompt("Enter your subject");
while(subject !==null){
    grade = parseInt(prompt('Enter your grade'));
    if (grade >=0){
        student['table'].push(grade)
    } else {
        grade = parseInt(prompt('Enter Correct Grade'));
    }
    subject = prompt("Enter your subject");
}
// Check Negative Marks
let negativeCounter=[];
student['table'].forEach( (item)=> {
    if (item < 4) {
        negativeCounter.push(item)
    }
});
if (negativeCounter.length === 0){
    alert('Студент переведен на следующий курс');
}
// Get Average Grades
const average = student['table'].reduce( ( p, c ) => p + c, 0 ) / student['table'].length;
if (average>7){
    alert('Студенту назначена стипендия');
}
