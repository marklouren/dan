
let switchingButton = document.getElementById('themeSwitch');
let btns = Array.prototype.slice.call (document.getElementsByClassName('btn-grey'));

/* Styles for Light Theme */
function lightTheme(){
    switchingButton.innerText = 'Switch to Dark Theme';
    document.getElementsByTagName('main')[0].classList.add('lightBackgroundImage');
    document.getElementsByTagName('table')[0].classList.add('lightBorder');
    btns.forEach((i)=>{i.classList.add('lightGrey');})
}

/* Styles for Dark Theme */
function darkTheme(){
    switchingButton.innerText = 'Switch to Light Theme';
    document.getElementsByTagName('main')[0].classList.remove('lightBackgroundImage');
    document.getElementsByTagName('table')[0].classList.remove('lightBorder');
    btns.forEach((i)=>{i.classList.remove('lightGrey');})

}

/* Switching Button */
let status = false; //dark theme flag

switchingButton.addEventListener('click', function() {
        if (status === false) {
            status = true;
            lightTheme();
            localStorage.setItem("theme", "light");
        } else {
            status = false;
            darkTheme();
            localStorage.removeItem("theme");

        }
    }
);

// Check DATA IN the LOCAL STORAGE:
if (localStorage.getItem('theme')=== 'light'){
    lightTheme();
    status = true;
}



