let inputPrice =document.getElementsByClassName('price_input')[0];
let priceBox =document.getElementsByClassName('price-box')[0];
let spanPrice = document.getElementsByClassName('price_value')[0];
let priceDelete = document.getElementsByClassName('delete')[0];
let mistakeDiv =  document.getElementsByClassName('mistake_js')[0];
let realPrice;

inputPrice.style.outlineColor="green";
inputPrice.addEventListener('focusout', function() {
    realPrice = inputPrice.value;
    if (realPrice !== "") {
        if (realPrice < 0) {
            inputPrice.style.borderColor = 'red';
            mistakeDiv.style.display = 'block';
        } else {
            inputPrice.style.borderColor = 'transparent';
            spanPrice.innerText = `$ ${realPrice}`;
            priceBox.style.display = "block";
            inputPrice.style.color = 'green';
            mistakeDiv.style.display = 'none';
        }
    }
});
priceDelete.addEventListener('click',()=>{
    priceBox.style.display="none";
    inputPrice.value ='';
    inputPrice.style.color='black';
});

