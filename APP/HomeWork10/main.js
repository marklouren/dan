// Initialization:

let passwordInput=document.getElementsByClassName('input_password')[0];
let passwordConfirm=document.getElementsByClassName('input_password_confirm')[0];
let passwordsFields = Array.prototype.slice.call(document.getElementsByTagName('input'));
let error=document.getElementsByClassName('error')[0];
let welcome = document.getElementsByClassName('welcome')[0];
let icons= Array.prototype.slice.call(document.getElementsByClassName('icon-password'));

// Show a password by clicking the icon:

for (let i=0; i<icons.length; i++){
    icons[i].addEventListener('click', ()=>{
        if (icons[i].classList.contains('fa-eye')){
            icons[i].classList.remove('fa-eye');
            icons[i].classList.add('fa-eye-slash');
            passwordsFields[i].type = 'password'
        }
         else {
            icons[i].classList.remove('fa-eye-slash');
            icons[i].classList.add('fa-eye');
            passwordsFields[i].type = 'text';
            }
    })
}
 // Validation

 function validation(){
    if (passwordInput.value === passwordConfirm.value){
        welcome.style.display ='block';
        error.style.display='none';
    } else {
        error.style.display='block';
        welcome.style.display ='none';
    }
 }

//confirmation by the Button click:
 document.getElementsByTagName('button')[0].addEventListener('click', validation);

