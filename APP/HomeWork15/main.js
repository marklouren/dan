
/* Anchor Texts */

jQuery(document).ready(function($) {

    $('a[href="#popular_posts"]').click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 1000);
    });

    $('a[href="#our_clients"]').click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 1000);
    });
    $('a[href="#top_rated"]').click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 1000);
    });
    $('a[href="#hot_news"]').click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 1000);
    });
});

/* Button Top */

const btn=$('.scroll-top');
btn.hide();
$(window).on('scroll', ()=>{
    if ($(window).scrollTop()>$(window).height()){
       btn.show()
    } else {btn.hide()}
});
btn.on('click', function(e){
    e.preventDefault();
    $('html, body').animate ({scrollTop:0}, 1000);
});

/* Toogle  */
let flag = true;
$(".hide_section").click(function(){
    if (flag === true){
        $(".posts-gallery").toggle("fast");
        $(".hide_section").text('Show Section');
        flag = false;
    } else {
        $(".posts-gallery").toggle("slow");
        $(".hide_section").text('Hide Section');
        flag = true;
    }
});