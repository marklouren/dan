function createNewUser() {
    let name = prompt('Enter your name');
    let surname = prompt('Enter your surname');
    let newUser = {
        name,
        surname,
        set firstName(newValue) {
            this.name = newValue;
        },
        get firstName(){
            return this.name;
        },
        set lastName(newValue) {
            this.surname = newValue;
        },
        get lastName() {
            return this.surname;
        },
        getLogin() {
            return (`${(this.firstName).toString()[0].toLowerCase()}${this.lastName.toString().toLowerCase()}`);
        },
    };
    return (newUser);
}

console.log(createNewUser().getLogin());
