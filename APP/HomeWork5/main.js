
function createNewUser() {
    let name = prompt('Enter your name');
    let surname = prompt('Enter your surname');
    let birthday = prompt('Enter your birthday date: dd.mm.yyyy');

    let newUser = {
        name,
        surname,
        birthday,
        getLogin() {
            return (`${(this.firstName).toString()[0].toLowerCase()}${this.lastName.toString().toLowerCase()}`);
        },
        getAge(){
            let birthday_mls = Date.parse((this.birthday).split(".").reverse().join("-"));
            let date = new Date().getTime();
            let fullAge=(date-birthday_mls);
            return (Math.floor(fullAge/31536000000)); //31536000000 mls in Year
        },
        getPassword(){
            return (`${this.name[0].toUpperCase()}${this.surname.toLowerCase()}${this.birthday.slice(-4)}`)

        }
    };
    return ( `Your age is: ${newUser.getAge()} and your password is: ${newUser.getPassword()}`);
}

console.log(createNewUser());
