import React, {Component} from 'react';
import CardItem from "../CardItem/CardItem";
import "./CardListStyle.scss";
// TEMP
import Modal from "../ModalPopup";

export default class CardList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="listItem-section">
                <h2 className="listItem-title">New Arrivals</h2>
                <div className="listItem-block">
                {this.props.ListData.map((x) => {
                        return (<div key={x.isbn} className="listItem-item">
                                <CardItem cardData={x}/>
                            </div>)})
                }
                </div>
            </div>
        )
    }
}