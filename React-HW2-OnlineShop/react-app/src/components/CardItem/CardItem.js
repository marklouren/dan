import React, {Component} from 'react';
import Modal from '../ModalPopup'
import LocalStorageManager from '../LocalStorage/Local-storage-manager'
import "./CardItemStyle.scss"

export default class CardItem extends Component {
    constructor(props){
        super(props);
        this.state={
            showModal:false,
            favorite:false
        };
        this.toggleToFavorite=this.toggleToFavorite.bind(this);
        this.openModal=this.openModal.bind(this);
    }

    // openModal(){
    //     this.setState({showModal: !this.state.showModal});
    // };
    openModal(boolean){
        this.setState({showModal:boolean});
        return(this.state);
    };

    addToCardFunction(){
        this.openModal(true)
    }

    toggleToFavorite(){
        this.setState({favorite: !this.state.favorite});
        if (localStorage.getItem(this.props.cardData.isbn) === null){
            LocalStorageManager.pushToLocalStorage(this.props.cardData.isbn, this.props.cardData)
        } else {
            LocalStorageManager.removeFromLocalStorage(this.props.cardData.isbn)
        }
    }



    render(){
        return(
            <>

                {(this.state.showModal)?<Modal modalAction={this.openModal} product={this.props.cardData}/>:null}
            <div className="cardItem">
                <div className=" cardItem-row cardItem-star" >
                    <div></div>
                    <div onClick={this.toggleToFavorite} className= {(this.state.favorite)?"cardItem-addToFavorite added":"cardItem-addToFavorite"}></div></div>
                <div className="cardItem-row cardItem-image-wrapper"><img className="cardItem-image" src={this.props.cardData.thumbnailUrl} alt="item image"/></div>
                <div className="cardItem-row"><span className="cardItem-description">Title:</span><span className="cardItem-title">{this.props.cardData.title}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">Price:</span><span className="cardItem-price">{this.props.cardData.price}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">ISBN:</span><span className="cardItem-isbn">{this.props.cardData.isbn}</span></div>
                <div className="cardItem-row"><span className="cardItem-description">Color:</span><span className="cardItem-color">{this.props.cardData.color}</span></div>
                <div className="cardItem-row"><button className="cardItem-btn" onClick={ ()=>this.addToCardFunction()}>Add to Card</button></div>
        </div>
           </>

        )
    }
}