import React, {Component} from 'react';
import isNil from 'lodash/fp/isNil';
import "./ModalPopupStyle.scss"
import LocalStorageManager from "../LocalStorage/Local-storage-manager";
export default class Modal extends Component {

    constructor(props) {
        super(props);
        this.product=this.props.product;
        this.modalAction=this.props.modalAction;
        this.onClickHandler=this.onClickHandler.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
        this.addBtnClickHandler =this.addBtnClickHandler.bind(this);
    }
    componentDidMount() {
        document.addEventListener('click', this.handleOutsideClick, false);
    }
    componentWillUnmount() {
        document.removeEventListener('click', this.handleOutsideClick, false);
    }
    handleOutsideClick(e) {
        if (!isNil(this.modal)) {
            if (!this.modal.contains(e.target)) {
                this.modalAction();
            }
        }
    }
    onClickHandler(){
        this.modalAction(false)
    }

    addBtnClickHandler(){
        LocalStorageManager.pushToLocalStorage('BasketItem:'+this.product.isbn, this.props.product)
        this.modalAction(false)
    }


    render() {

        return (

            <div  className="modal-window">
                <div className="modal-window-body-wrapper" ref={node => (this.modal = node)}>

                    <div className="modal-window-header">
                        <div className="modal-window-close" onClick={this.onClickHandler}>x</div>
                    </div>
                    <div className="modal-window-body">
                        <div className="modal-window-content">
                            <p className="modal-window-content-title"> Do you want to add this Item to Basket?</p>
                            <p><span className="modal-window-content-point">Book Name:</span> {this.props.product.title}</p>
                            <p><span className="modal-window-content-point">Book Price:</span> {this.props.product.price}</p>
                        </div>

                        <div className="modal-window-btn">
                            <button className="modal-btn" onClick={this.addBtnClickHandler}>Add to Basket</button>
                            <button className="modal-btn" onClick={this.onClickHandler}>close</button>
                        </div>
                    </div>
                </div>

            </div>



        )

    }
}