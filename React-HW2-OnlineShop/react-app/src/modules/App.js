import React, {Component} from 'react';
import './App.scss';
import CardList from '../components/CardList/CardList'
import Services from "../services/Services";


export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listItems: [],
        };
    }

   async componentDidMount() {
        const List = await Services.getList();
       this.setState({listItems: List.ItemList});
    }

    render(){
            return(
        <>
          <CardList ListData={this.state.listItems}/>
          </>
    )}
}
