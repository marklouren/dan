/* Small 50 grn, 20 cal, big - 100 grn, 40 cal
must:
cheese -10 grn 20 cal
salad - 20 grn 5 cal
potato - 15 grn 10 cal
optional:
seasoning - 15 grn 0 cal
mayo - 20  grn 5 cal

Program Count Cost of a burger and calories ES 5 using class
Error handling
 */

/**
*Класс, объекты которого описывают параметры гамбургера.
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
 * size - string
 * stuffing - array
 */
function Hamburger(size, stuffing) {
    // needed in order to call methods before their definition
    this.structure=[];
    this.topping =[];
    this.calcSize=[];
    this.hamburgerComposition = [];
    //  our arguments
    this.size=size;
    this.stuffing=stuffing;
        for (let i=0; i<this.stuffing.length;i++){
            if (this.stuffing[i]!=='cheese'&&this.stuffing[i]!=='potato'&&this.stuffing[i]!=='salad'){
                throw new HamburgerException ('You must have only: cheese, salad or potato among stuffing');
            }
        }

            if (this.size !== 'small'&&this.size !=='large'){
                throw new HamburgerException('Size should be only a string with values: small or large')
            }
            if (this.size === 'small') {
                this.SIZE_SMALL = {name: 'small', price: 50, cal: 20};
                this.calcSize.push(this.SIZE_SMALL);
            }
            if (this.size === 'large') {
                this.SIZE_LARGE = {name: 'large', price: 100, cal: 40};
                this.calcSize.push(this.SIZE_LARGE);
            }


            if (!Array.isArray(this.stuffing)||this.stuffing.length === 0){
                throw new HamburgerException ('Stuffing should be an array with at least one element')
            }
            if (this.stuffing.includes('cheese')) {
                this.STUFFING_CHEESE = {name: 'cheese', price: 10, cal: 20};
                this.structure.push(this.STUFFING_CHEESE)
            }
            if (this.stuffing.includes('salad')) {
                this.STUFFING_SALD = {name: 'salad', price: 20, cal: 5};
                this.structure.push(this.STUFFING_SALD)
            }
            if (this.stuffing.includes('potato')) {
                this.STUFFING_POTATO = {name: 'potato', price: 15, cal: 10};
                this.structure.push(this.STUFFING_POTATO)
            }

    }


/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные. // []
 * @param topping  Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping){ //[]
    this.topping=topping;
    this.toppingArray=[];
    if (this.topping.length>2){
        throw new HamburgerException ('You can not have more than 2 elements in toppings')
    }
    if (this.topping.includes('mayo')){
        this.TOPPING_MAYO = {name:'mayo', price: 20, cal:5};
        this.toppingArray.push( this.TOPPING_MAYO);
    }
    if (this.topping.includes('spice')){
        this.TOPPING_SPICE=  {name:'spice', price: 20, cal:5};
        this.toppingArray.push( this.TOPPING_SPICE);
    }
    return this.toppingArray
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function(topping){
    this.topping=topping;

    if ( this.topping.includes('mayo')){
        delete this.TOPPING_MAYO;
        this.toppingArray=this.toppingArray.filter((item)=>{return item.name !=='mayo';});
    }
    if ( this.topping.includes('spice')){
        delete this.TOPPING_MAYO;
        this.toppingArray=this.toppingArray.filter((item)=>{return item.name !=='spice';});
    }
    return this.toppingArray;
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function (){
    return this.toppingArray;
};
/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function (){
    return this.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
        for (let i = 0; i < (this.structure).length; i++) {
            this.hamburgerComposition.push(this.structure[i].name);
        }
        if (this.topping.length > 0) {
            this.hamburgerComposition = this.hamburgerComposition.concat(this.topping);
        }
        return this.hamburgerComposition;

};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    this.price = 0;
    this.price+=this.calcSize[0].price;  //size
    this.structure.forEach((item)=>{this.price+=item.price;}); //stuffing
    this.toppingArray.forEach((item)=>{this.price+=item.price;}); // toppings
    return this.price
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function (){
    this.cal = 0;
    this.cal+=this.calcSize[0].cal;  //size
    this.structure.forEach((item)=>{this.cal+=item.cal;}); //stuffing
    this.toppingArray.forEach((item)=>{this.cal+=item.cal;}); // toppings
    return this.cal
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException (message) {
    this.name= 'Hamburger Exception Function';
    this.message = message;
}
//HamburgerException.prototype = Error.prototype;

// TEST Function:
try {
    // create a second burger
    secondBurger = new Hamburger("large", ['cheese', 'potato', 'salad']);
    // console.log(secondBurger);
    // add Topping
    console.log( `ADD TOPPINGS:`);
    console.log( secondBurger.addTopping(['mayo', 'spice'])); // Why does not displays in string:  console.log(`Add toppings: ${secondBurger.addTopping(['mayo', 'spice'])}');
    //remove Topping
    console.log("Remove Toppings - Remove Spice:");
    console.log(secondBurger.removeTopping ('spice'));
     //See the list of Toppings
    console.log(`See the list of Toppings - Mayo:`);
    console.log(secondBurger.getToppings());
    //узнать размер бургера
    console.log(`Burger Size: ${secondBurger.getSize()}`);
    //узнать начинку бургера
    console.log(`Burger Stuffing: ${secondBurger.getStuffing()}`);
    //узнать цену
    console.log(`Burger Price: ${secondBurger.calculatePrice()} Tugriks`);
    //узнать калорийность
    console.log(`Burger Calories: ${secondBurger.calculateCalories()}`);

    // Check Errors:

    //Added Not Required Element
    thirdBurger = new Hamburger('small', ['butter', 'potato']);
    // At least one Element
    thirdBurger = new Hamburger('small', []);
    // Not required size
    thirdBurger = new Hamburger('s', ['salad']);


} catch (e){
console.error(e)
}
