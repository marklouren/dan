const urlPosts='https://jsonplaceholder.typicode.com/posts';
const urlUsers='https://jsonplaceholder.typicode.com/users';
// Initialization
const postSection =$('.twitter-news-feed');
const publishBtn =$('.add-post-button');
const modalWindow =$('.add-post-modal');

// Variables
let listPosts, listUsers, twitterItem;
let postCounter =0;

// Display All Posts on the Page (via get jQuery)
//$.get(urlPosts, (x)=>{listPosts=x});
//$.get(urlUsers, (x)=>{listUsers=x});

$.ajax({
    url: urlPosts,
    dataType: "json",
    async:false,
    success: function (data){
        listPosts=(data)
    }
});
$.ajax({
    url: urlUsers,
    dataType: "json",
    async:false,
    success: function (data){
        listUsers=(data)
    }
});


postCounter =listPosts.length;

// Add one Post from Request
function addPost(post, userObj){
    twitterItem = document.createElement('div');
    twitterItem.classList.add('twitter-card');
    twitterItem.dataset.userId = `${ userObj.id}`;
    twitterItem.dataset.postId = `${post.id}`;
    twitterItem.innerHTML = `<div class="twitter-card-post">
            <div class="twitter-card-post-user-credentials">
                <div class="twitter-card-post-user-name twitter-card-post-cred">${ userObj.name.split(' ')[0]}</div>
                <div class="twitter-card-post-user-surname  twitter-card-post-cred">${userObj.name.split(' ')[1]}</div>
                <div class="twitter-card-post-user-email  twitter-card-post-cred">${userObj.email}</div>
            </div>
            <div class="twitter-card-post-all" data-card-post="${post.id}" data-user-id="${userObj.id}" >
                <div class="twitter-card-post-title" contentEditable="false">${post.title}</div>
                <div class="twitter-card-post-body" contentEditable="false"> ${post.body}</div>
                <div class="twitter-card-post-buttons">
                    <button class="twitter-card-btn twitter-card-post-change">Change</button>
                    <button class="twitter-card-btn twitter-card-post-publish">Publish</button>
                    <button class="twitter-card-btn twitter-card-post-delete">Delete</button>
                </div>
            </div>
        </div>`;
    postSection.prepend(twitterItem);
}
// add All Posts
function addPosts(posts,users) {
    for (let i = 0; i < users.length; i++) {
        let userPosts = (posts.filter(post => post.userId === listUsers[i].id));
        userPosts.forEach(x => {
            addPost(x, users[i])
        });
    }
}
// Launch a function
addPosts(listPosts,listUsers);

// NEW POST
// Display Modal
publishBtn.on('click', ()=>{
    modalWindow.css("display", "block");
});
// hide Modal
$('.twitter-modal-close').on('click', ()=>{
    modalWindow.css("display", "none");
});
// add a new Post to HTML

// Event Handler for Each type of Events:

postSection.on('click', function(event) {
    //initialization
    let elements = $(event.target).parents(".twitter-card-post-all").children().toArray();
    let postNumber= $(event.target).parents(".twitter-card-post-all").data('card-post');
    let userNumber = $(event.target).parents(".twitter-card-post-all").data('user-id');
    // edit post
    if ($(event.target).hasClass('twitter-card-post-change')){
        elements[0].contentEditable=true;
        elements[0].className= "twitter-card-post-title changed";
        elements[1].contentEditable=true;
        elements[1].className = "twitter-card-post-body changed";
    }
    if ($(event.target).hasClass('twitter-card-post-publish')){
        elements[0].contentEditable=false;
        elements[0].className= "twitter-card-post-title";
        elements[1].contentEditable=false;
        elements[1].className = "twitter-card-post-body";

        // SEND EVENT TO SERVER WITH POST ID
        fetch('https://jsonplaceholder.typicode.com/posts/'+postNumber, {
            method: 'PUT',
            body: JSON.stringify({
                id: postNumber,
                title:elements[0].innerText,
                body: elements[1].innerText,
                userId: userNumber
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then(json => console.log(json));
    }
     // DELETE CARD IN HTML
    if ($(event.target).hasClass('twitter-card-post-delete')){
        //delete Post on Server
        fetch("https://jsonplaceholder.typicode.com/posts/"+postNumber, {
            method: 'DELETE'
        }).then(response=>{console.log('post Deleted')});
        // delete post on webPage
        $('.twitter-card').filter(function(){
            return $(this).data('post-id') === postNumber;
        }).remove();
    }
    //Add Twitter CARD in HTML  - How to Add new Event Listener to NEW DOM ELEMENT?
    if ($(event.target).hasClass('twitter-card-post-add')){
        let newPost ={};
        postCounter+=1; // simulation post ID
        newPost.title=elements[1].innerText;
        newPost.body=elements[2].innerText;
        newPost.id=postCounter;
        addPost(newPost, listUsers[0]);
        modalWindow.css("display", "none");

        // Add a new Post on the server:
        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify({
                title: newPost.title,
                body: newPost.body,
                userId:listUsers[0]
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then(json => console.log(json))
    }
});

