
// REQUESTS
async function ajaxGet() {
    let fetchRequest = await fetch('https://api.ipify.org/?format=json');
    if (fetchRequest.ok) {
        let fetchAnswer = await fetchRequest.json();
        let object = await fetch('http://ip-api.com/json/'+fetchAnswer.ip+"?fields=continent,country,regionName,city,district");
        let objectJson= await object.json();
        showInfo(objectJson);
    }
    else {
        console.log("Ошибка HTTP: " + fetchRequest.status);
    }
}
// BTN LOGIC - REQUEST ON CLICK
function btnUI() {
    let showBtn =document.getElementsByClassName('btn-get-ip')[0];
    let infoSection =document.getElementsByClassName('info-ip')[0];
    showBtn.addEventListener('click', ()=>{
        infoSection.classList.toggle('hide');
        ajaxGet();
    })
}
// Logic adding info into HTML
function showInfo(objectJson){
let list=document.getElementsByClassName('info-ip-text');
let listArray= Array.prototype.slice.call(list);
let objArray=Object.values(objectJson);
for (let i=0; i<listArray.length; i++){
    listArray[i].innerText=objArray[i];}
}

// Launch a Function
btnUI();