document.getElementById('#listMovies').addEventListener('click', ()=>{
    ajaxGet();
});

let container = document.getElementsByClassName('container')[0];
let ArrayLinksToCharacters =[];
function ajaxGet(){
    let request = new XMLHttpRequest();
    request.responseType='json'; //  какой тип ответа ожидаем от сервера
    request.onreadystatechange = function() {
        if (this.readyState === 4 && request.status === 200) {
            let receivedValue = this.response.results;
            //console.log(this.response.results);
            // ADD MOVIES LIST
            for (let i = 0; i < receivedValue.length; i++) {
                ArrayLinksToCharacters.push(receivedValue[i].characters);
                let movieItem = document.createElement('div');
                movieItem.classList.add('movie');
                movieItem.setAttribute('data-name', receivedValue[i].title);
                movieItem.innerHTML = `
         <h3 class="movie-item movie-id">${receivedValue[i].episode_id}</h3>
        <div class="movie-item movie-name">${receivedValue[i].title}</div>
        <p class="movie-item movie-opening-crawl">${receivedValue[i].opening_crawl}</p>
        <div class="movie-item movie-heroes">
            <h3 class="movie-item movie-heroes-title">List of Characters:</h3>
            <ul class="movie-heroes-list">
            </ul>
        </div>`;
                container.appendChild(movieItem);
                let movieList=document.getElementsByClassName('movie-heroes-list')[i];
                /*===========================   ADDED LIST OF MOVIES XML */
                for (let b=0; b<ArrayLinksToCharacters[i].length; b++) {
                    let nameRequest = new XMLHttpRequest();
                    nameRequest.open('GET', ArrayLinksToCharacters[i][b]);
                    nameRequest.onreadystatechange = function() {
                        if ( nameRequest.readyState === 4 &&  nameRequest.status === 200) {
                            const res = JSON.parse( nameRequest.response);
                            let person =document.createElement('li');
                            person.innerText=res.name;
                            movieList.appendChild(person);
                        }
                    };
                    nameRequest.send()
                    }

                }


        }

    };
    request.open("get","https://swapi.co/api/films/", true );
    request.send();

}

