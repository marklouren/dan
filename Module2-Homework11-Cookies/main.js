/*
По нажатию на кнопку Contact us, в cookies браузера должны сохраняться следующие данные:
название cookie - experiment, значение - novalue, истекает через 5 минут.
название cookie - new-user, значение - true или false в зависимости от того,
 существует ли уже такая запись. При первом нажатии значение должно быть true,
 при втором и последующих нажатиях - false.
 */

let contactBtn=document.getElementsByClassName('js_contact-us')[0];

contactBtn.addEventListener('click', ()=>{
    sendCookies();
    sendCookieNewUser();
});

function sendCookies(){
    let date = new Date(Date.now() + 300000);
    date = date.toUTCString();
    document.cookie = "user=John; expires=" + date;
    document.cookie = "new-user=true";

}
function sendCookieNewUser(){
  let checkCookie =document.cookie.split('; ');
    if (checkCookie.includes("new-user=true")){
        document.cookie = "new-user=false";
    }
}
