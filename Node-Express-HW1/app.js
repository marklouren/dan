let express = require('express');
let bodyParser = require('body-parser');
let path = require('path');
let app =express();
// let logger =(req, res, next)=>{
//     console.log('Logging...');
//     next();
// };
//app.use(logger);
// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//Set Static Path
//app.use(express.static(path.join(__dirname, 'public')));

let people = [
    {name:'Jeff',
    age:30},
    { name:'Sara',
    age:25}
];

app.get('/', (req, res)=>{
res.json(people);
});
app.listen(3000, ()=>{
console.log('Server Started on Port 3000')});
